import { useState } from "react";
import { ReportPayload } from "@libraries/api";

function useReport() {
  const [loading, setLoading] = useState<boolean>(false);
  const [data, setData] = useState<any[]>([]);

  async function fetchReport() {
    setLoading(true);
    try {
      const response = await ReportPayload.fetchReport();
      setData(response.data.data);
      setLoading(false);
    } catch (error) {
      setLoading(false);
    }
  }

  return {
    data,
    loading,
    fetchReport,
  };
}

export default useReport;
