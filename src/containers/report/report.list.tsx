import {
  TableColumnProps,
  Typography,
  Table,
  TableColumnsType,
  Row,
  Col,
  Badge,
  Button,
} from "antd";
import { Content } from "antd/lib/layout/layout";
import { useContext, useEffect, useState } from "react";

import { Document, Download } from "@components/icons";
import { Link } from "react-router-dom";
import useReport from "./hooks/useReport";
import { formatDate } from "@helpers/datetime";
import {
  ModalDownloadContext,
  withModalDownload,
} from "@components/modals/modal-context";

interface IDataReport {
  id: string;
  username: string;
  unit: string;
  category: string;
  program: string;
  date: string;
  method: string;
  style: string;
  status: string;
}

const DATA: IDataReport[] = [
  {
    id: "1",
    username: "Bessie Cooper",
    unit: "426426",
    category: "Design",
    program: "UI UX for Beginners",
    date: "2021-05-07",
    method: "Online Classroom",
    style: "Presentation",
    status: "On School Development",
  },
  {
    id: "2",
    username: "Bessie Cooper",
    unit: "426426",
    category: "Design",
    program: "UI UX for Beginners",
    date: "2021-05-07",
    method: "Online Classroom",
    style: "Presentation",
    status: "On School Development",
  },
  {
    id: "3",
    username: "Bessie Cooper",
    unit: "426426",
    category: "Design",
    program: "UI UX for Beginners",
    date: "2021-05-07",
    method: "Online Classroom",
    style: "Presentation",
    status: "On School Design",
  },
  {
    id: "4",
    username: "Bessie Cooper",
    unit: "426426",
    category: "Design",
    program: "UI UX for Beginners",
    date: "2021-05-07",
    method: "Online Classroom",
    style: "Presentation",
    status: "On Learning Analyst",
  },
  {
    id: "5",
    username: "Bessie Cooper",
    unit: "426426",
    category: "Design",
    program: "UI UX for Beginners",
    date: "2021-05-07",
    method: "Online Classroom",
    style: "Presentation",
    status: "On School Development",
  },
];

function ReportList() {
  const { makeDownload } = useContext(ModalDownloadContext);

  const COLUMNS = [
    {
      title: "Username",
      dataIndex: "username",
      // fixed: "left",
      width: 200,
    },
    {
      title: "Unit",
      dataIndex: "unit_name",
      // fixed: "left",
      width: 100,
    },
    {
      title: "Category",
      dataIndex: "cfu_fu",
      // fixed: "left",
      width: 150,
    },
    {
      title: "Program",
      dataIndex: "training_name",
      // fixed: "left",
      width: 300,
    },
    {
      title: "Date",
      dataIndex: "created_at",
      // fixed: "left",
      width: 150,
      render(date: string) {
        return <span>{formatDate(date)}</span>;
      },
    },
    {
      title: "Metode",
      dataIndex: "delivery_method",
      // fixed: "left",
      width: 150,
    },
    {
      title: "Style",
      dataIndex: "learning_styles",
      // fixed: "left",
      width: 150,
    },
    {
      title: "Status",
      dataIndex: "status_name",
      // fixed: "left",
      width: 300,
      render(item: string) {
        const colors = {
          background: "#CDFFCD",
          color: "#007F00",
        };

        switch (item) {
          case "On School Design":
            Object.assign(colors, {
              background: "#D5D5F9",
              color: "#5A5AFF",
            });
            break;
          case "On Learning Analyst":
            Object.assign(colors, {
              background: "#FFECCC",
              color: "#965E00",
            });
            break;
        }

        return (
          <span
            style={{
              display: "inline-block",
              padding: ".3rem 1rem",
              borderRadius: "3rem",
              ...colors,
            }}
          >
            {item}
          </span>
        );
      },
    },
    {
      title: "Actions",
      dataIndex: "id",
      fixed: "right",
      width: 150,
      render(id: string) {
        return (
          <div style={{ display: "flex", flexDirection: "row", width: "100%" }}>
            <Button type="link" href={`/submit-form/preview/${id}`}>
              <Document />
            </Button>
            <Button type="link" onClick={() => makeDownload(id)}>
              <Download />
            </Button>
          </div>
        );
      },
    },
  ];
  const [selectedRowKeys, setSelectedRowKeys] = useState<any>();

  const { loading, data, fetchReport } = useReport();

  // initialize
  useEffect(() => {
    fetchReport();
  }, []);

  return (
    <>
      <Typography children="Report" className="title" />
      <Content className="content-page">
        {data.length > 0 && (
          <Table
            columns={COLUMNS as any}
            dataSource={data}
            rowSelection={{
              selectedRowKeys,
              onChange: (selectedRowKeys) => {
                setSelectedRowKeys(selectedRowKeys);
              },
            }}
            scroll={{ x: 1500, y: 1500 }}
          />
        )}
      </Content>
    </>
  );
}

export default withModalDownload(ReportList);
