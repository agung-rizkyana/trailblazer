import { useState } from "react";
import { ILogin } from "../libraries/login";
import AuthPayload from "@libraries/api/auth";
import AuthHelper from "@helpers/auth";

function useLogin() {
  const [loading, setLoading] = useState<boolean>(false);
  const doLogin = async (data: ILogin) => {
    setLoading(true);
    try {
      const response = await AuthPayload.login(data);

      // set authorization
      AuthHelper.setToken(response.headers["authorization"]);
      return [response.data, null];
    } catch (error) {
      return [null, error];
    } finally {
      setLoading(false);
    }
  };

  return {
    doLogin,
    loading,
  };
}

export default useLogin;
