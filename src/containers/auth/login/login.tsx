import { Button, Form, Input, Alert, Typography } from "antd";

import { EyeInvisibleOutlined, EyeTwoTone } from "@ant-design/icons";
import { ILogin, initialValues } from "./libraries/login";
import useLogin from "./hooks/useLogin";
import { useHistory } from "react-router-dom";

function Login() {
  const history = useHistory();
  const [form] = Form.useForm();
  const { doLogin, loading } = useLogin();
  const handleSubmit = async (values: ILogin) => {
    const [data, error] = await doLogin(values);
    if (error) {
      console.log("ada error :: ", error);
    }

    history.push("/submit-form");
  };

  const handleSubmitFailed = (e: any) => {
    console.log("error :: ", e);
  };

  return (
    <Form
      className="auth-login"
      form={form}
      layout="vertical"
      initialValues={initialValues}
      onFinish={handleSubmit}
      onFinishFailed={handleSubmitFailed}
    >
      <Typography className="sub-title" children="Login to your account" />
      <Form.Item label="Email" rules={[{ required: true }]} name="email">
        <Input
          type="email"
          size="large"
          name="email"
          placeholder="Input your email"
        />
      </Form.Item>
      <Form.Item label="Password" rules={[{ required: true }]} name="password">
        <Input.Password
          size="large"
          name="password"
          placeholder="Input your password"
          iconRender={(visible: boolean) =>
            visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
          }
        />
      </Form.Item>
      <Form.Item>
        <Button
          size="large"
          htmlType="submit"
          type="primary"
          children="Login"
          loading={loading}
        />
      </Form.Item>
    </Form>
  );
}

export default Login;
