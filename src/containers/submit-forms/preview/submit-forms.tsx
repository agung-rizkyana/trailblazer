import {
  LEARNING_STYLE,
  REQUIREMENTS,
  DIVISI,
  CFUFU,
  JUMLAH_PESERTA,
  POSISI,
  BUDGET,
  USIA,
  LAMA_PELAKSANAAN,
  LOKASI_KERJA,
} from "./libraries/submit-form";

import {
  Button,
  Layout,
  Row,
  Col,
  Form,
  Input,
  Radio,
  Checkbox,
  Typography,
  Select,
  DatePicker,
  Space,
} from "antd";
import { IRadio } from "@interfaces/iradio";

import Info from "@components/info";

import { withModal, useMyModal } from "@components/modals/modal.context";
import { useHistory, useParams } from "react-router-dom";
import MultipleInput from "./libraries/multiple.input";
import { ISubmitForm } from "@interfaces/isubmit-form";

import useSubmit from "./hooks/useSubmit";
import { useSubmitForm } from "./libraries/submit-form.context";

import { ModalSubmit, ModalConfirm } from "@components/modals";
import { useContext, useEffect, useState } from "react";
import useCfu from "./hooks/useCfu";
import { SelectValue } from "antd/lib/select";

import { Save, Delete, Download, Edit } from "@components/icons";

import moment from "moment";
import useDetail from "./hooks/useDetail";
import {
  ModalDownloadContext,
  withModalDownload,
} from "@components/modals/modal-context";

const { Content } = Layout;

function disabledDate(current: any) {
  // Can not select days before today and today
  return current && current < moment().endOf("day");
}

function SubmitForm() {
  const { makeDownload } = useContext(ModalDownloadContext);

  const [showSubmit, setShowSubmit] = useState<boolean>(false);
  const [showConfirm, setShowConfirm] = useState<boolean>(false);
  const [school, setSchool] = useState<string>("");
  const [payload, setPayload] = useState<ISubmitForm>({} as ISubmitForm);
  const { setData } = useSubmitForm();
  const [form] = Form.useForm();

  const { fetchDetail, detail } = useDetail();
  const { id } = useParams() as { id: string };

  // load detail
  useEffect(() => {
    if (id) {
      fetchDetail(id);
    }
  }, [id]);

  // set value to the form
  useEffect(() => {
    console.log("current detail :: ", detail);

    const keys = Object.keys(detail);
    const fields: { name: string; value: any }[] = [];
    keys.forEach((key: string) => {
      fields.push({
        name: key,
        value: detail[key as keyof ISubmitForm],
      });
    });

    console.log(fields);
    // form.setFields(fields);
  }, [detail]);

  const { failed, succces, confirm, close, submit } = useMyModal();
  const { loading, update } = useSubmit();
  const { fetchCfuFunctionName, fetchCfuByFunctionUnit, dataDivisi, dataFu } =
    useCfu();

  // fetch divisi
  useEffect(() => {
    fetchCfuFunctionName();
  }, []);

  useEffect(() => {
    form.setFields([
      {
        name: "cfu_fu",
        value: null,
      },
    ]);
    console.log("masuk data divisi");
  }, [form.getFieldValue("division")]);

  const history = useHistory();

  const handleSubmit = async () => {
    setShowSubmit(true);
    const unit_name = form.getFieldValue("unit_name");
    const makePayload = Object.assign(detail, {
      unit_name,
      status: 0,
      is_submitted: true,
    });
    setPayload(makePayload);
  };

  const handleFailed = () => {
    failed({
      onOk: close,
    });
  };

  const makeSubmit = async () => {
    setShowSubmit(false);
    setShowConfirm(true);
  };

  const makeUpdate = async () => {
    setShowConfirm(false);
    await update(payload, id);
    succces({
      onOk: close,
    });
  };

  return (
    <>
      <Typography children="Preview Form" className="title" />
      <Content style={{ margin: "1.5rem 0" }}>
        <Row gutter={12} align="middle">
          <Col span={16} className="text-right">
            <Button
              type="link"
              children={
                <>
                  <Save />
                  <Typography className="text-danger" children="Save" />
                </>
              }
            />
            <Button
              type="link"
              children={
                <>
                  <Delete />
                  <Typography className="text-danger" children="Delete" />
                </>
              }
            />
            <Button
              type="link"
              children={
                <>
                  <Download />
                  <Typography className="text-danger" children="Download" />
                </>
              }
              onClick={() => makeDownload(id)}
            />
            <Button
              type="link"
              children={
                <>
                  <Edit />
                  <Typography className="text-danger" children="Edit" />
                </>
              }
            />
          </Col>
          <Col span={8}>
            <Button
              block
              size="large"
              type="primary"
              children="Submit"
              className="btn-success"
              onClick={handleSubmit}
            />
          </Col>
        </Row>
      </Content>
      <Content className="content-page">
        <Form
          form={form}
          layout="vertical"
          onFinish={(values: ISubmitForm) => setShowSubmit(true)}
          onFinishFailed={handleFailed}
        >
          <ModalSubmit
            visible={showSubmit}
            onOk={() => makeSubmit()}
            onCancel={() => setShowSubmit(false)}
          />

          <Row gutter={12}>
            <Col span={12}>
              <Form.Item
                label="Divisi"
                name="division"
                rules={[
                  {
                    min: 1,
                    required: true,
                  },
                ]}
              >
                {detail.division_name}
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                label="CFU/FU"
                name="cfu_fu"
                rules={[
                  {
                    min: 1,
                    required: true,
                  },
                ]}
              >
                {detail.function_unit_name}
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <Info
                label="WHY"
                tooltipText="Contoh: Gangguan berulang Indihome melebihi batas maks. 50%"
              />
            </Col>
            <Col span={24} className="tb-form-item">
              <Typography>
                Jelaskan <span className="text-bold">latar belakang</span>{" "}
                kebutuhan program pelatihan?
              </Typography>
              {/* <MultipleInput name="whys" itemName="why" /> */}
              {detail.why}
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <Info
                label="WHAT"
                tooltipText="Contoh: Meningkatkan penyelesaian gangguan berulang IndiHome menjadi 45%"
              />
            </Col>
            <Col span={24} className="tb-form-item">
              <Typography>
                Jelaskan <span className="text-bold">target</span> yang hendak
                dicapai pada level <span className="text-bold">organisasi</span>
                ?
              </Typography>
              {detail.organization_what}
            </Col>
          </Row>
          <Row>
            <Col span={24} className="tb-form-item">
              <Typography>
                Jelaskan <span className="text-bold">target</span> yang hendak
                dicapai pada level <span className="text-bold">individu</span>?
              </Typography>
              {detail.individual_what}
            </Col>
          </Row>
          <Row gutter={12}>
            <Col span={24}>
              <Info
                label="Informasi lain yang dibutuhkan"
                tooltipText="Informasi lain yang perlu diketahui baik dari Unit Bisnis maupun Individu"
              />
            </Col>
            <Col span={12}>
              <Typography className="text-bold">Dari Unit Bisnis</Typography>
            </Col>
            <Col span={12}>
              <Typography className="text-bold">Dari Individu</Typography>
            </Col>
            <Col span={12}>
              <Form.Item name="ubis_participants" label="Jumlah Peserta">
                {detail.ubis_participants}
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item label="Posisi" name="individual_band">
                {detail.individual_band}
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item label="Budget" name="ubis_budget">
                {detail.ubis_budget}
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item label="Rentang Usia" name="individual_age">
                {detail.individual_age}
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item label="Lama Pelaksanaan" name="ubis_days">
                {detail.ubis_days}
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item label="Lokasi Kerja" name="individual_unit">
                {detail.individual_unit}
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item label="Informasi lainnya" name="ubis_other_info">
                {detail.ubis_other_info}
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item label="Informasi lainnya" name="individual_other_info">
                {detail.individual_other_info}
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={12}>
            <Col span={24}>
              <Info
                label="USULAN PROGRAM PELATIHAN"
                tooltipText={
                  <Space align="start" direction="vertical">
                    <Typography>
                      Judul pelatihan bisa berubah sesuai dengan hasil
                      klarifikasi lebih lanjut
                    </Typography>
                    <Typography className="text-bold">Contoh</Typography>
                    <div>
                      <Typography className="text-bold">
                        Kunci Sukses :{" "}
                      </Typography>
                      <Typography>
                        Peningkatan knowledge dalam penanganan gangguan Indihome
                      </Typography>
                    </div>
                    <div>
                      <Typography className="text-bold">
                        Program Strategis :{" "}
                      </Typography>
                      <Typography>
                        Peningkatan KPI Laten Divisi Consumer
                      </Typography>
                    </div>
                  </Space>
                }
              />
            </Col>
            <Col span={24} className="tb-form-item">
              <Typography>
                Indikator apa saja yang menjadi{" "}
                <span className="text-bold">kunci sukses</span> program
                pelatihan?
              </Typography>

              {detail.success_keys}
            </Col>
          </Row>
          <Row gutter={12}>
            <Col span={12}>
              <Form.Item name="training_name" label="Nama Pelatihan">
                {detail.training_name}
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item name="training_date" label="Tanggal Pelatihan">
                {detail.training_date}
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <Form.Item
                name="strategic_plan"
                label={
                  <Typography>
                    Jelaskan{" "}
                    <span className="text-bold">
                      program strategis perusahaan
                    </span>{" "}
                    yang akan didukung oleh program pelatihan ini?
                  </Typography>
                }
              >
                {detail.strategic_plan}
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <Info
                label="GOALS"
                tooltipText={
                  <Space direction="vertical" align="start">
                    <Typography className="text-bold">Contoh</Typography>
                    <div>
                      <Typography className="text-bold">
                        Tujuan pelatihan Unit :{" "}
                      </Typography>
                      <Typography>
                        Peserta berkontribusi sebesar 50% dalam pemenangan
                        proyek di enterprise dengan implementasi kemampuan
                        presentasi selama 3 hari kelas online dan 2 bulan masa
                        pemantauan
                      </Typography>
                    </div>
                    <div>
                      <Typography className="text-bold">
                        Tujuan pelatihan individu :{" "}
                      </Typography>
                      <Typography>
                        Peserta mendemonstrasikan kemampuan dalam presentasi
                        proyek Enterprise kepada klien
                      </Typography>
                    </div>
                    <div>
                      <Typography>
                        <span className="text-bold">Skill : </span> Peserta
                        diharapkan mampu mengimplementasikan kemampuan
                        presentasi dengan baik
                      </Typography>
                    </div>
                  </Space>
                }
              />
            </Col>
            <Col span={24}>
              <Form.Item
                name="ubis_goal"
                label={
                  <Typography>
                    Jelaskan <span className="text-bold">tujuan pelatihan</span>{" "}
                    yang ingin dicapai terhadap{" "}
                    <span className="text-bold">unit</span>?
                  </Typography>
                }
              >
                {detail.ubis_goal}
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <Form.Item
                name="individual_goal"
                label={
                  <Typography>
                    Jelaskan <span className="text-bold">tujuan pelatihan</span>{" "}
                    yang ingin dicapai terhadap{" "}
                    <span className="text-bold">individu</span>?
                  </Typography>
                }
              >
                {detail.individual_goal}
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={24} className="tb-form-item">
              <Typography>
                Jelaskan apa saja{" "}
                <span className="text-bold">skill yang ingin dikuasai</span>{" "}
                oleh peserta setelah mengikuti pelatihan ini?
              </Typography>

              {detail.objective_skills}
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <Info
                label="REQUIREMENT"
                tooltipText="Modul apa saja yang dibutuhkan dalam pelatihan"
              />
            </Col>
            <Col span={24}>
              <Form.Item
                name="delivery_method"
                label={
                  <Typography>
                    Jelaskan{" "}
                    <span className="text-bold">
                      kebutuhan materi pelatihan?
                    </span>
                  </Typography>
                }
              >
                {detail.delivery_method}
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <Form.Item
                name="learning_styles"
                label={
                  <Typography>
                    <span className="text-bold">Learning style</span> yang
                    dibutuhkan pada program pelatihan?
                  </Typography>
                }
              >
                {detail.learning_styles}
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </Content>
      <ModalConfirm
        visible={showConfirm && Boolean(form.getFieldValue("unit_name"))}
        school={form.getFieldValue("unit_name")}
        onCancel={() => setShowConfirm(false)}
        onOk={() => makeUpdate()}
      />
    </>
  );
}

const PreviewContainer = withModal(SubmitForm);

export default withModalDownload(PreviewContainer);
