import { ISubmitForm } from "@interfaces/isubmit-form";
import { SubmitPayload } from "@libraries/api";
import { useState } from "react";

function useDetail() {
  const [loading, setLoading] = useState<boolean>(false);
  const [detail, setDetail] = useState<ISubmitForm>({} as ISubmitForm);

  async function fetchDetail(id: string) {
    setLoading(true);
    try {
      const response = await SubmitPayload.detail(id);
      setDetail(response.data.data as ISubmitForm);
    } catch (error) {
      setLoading(false);
      return error;
    }
  }

  return {
    loading,
    detail,
    fetchDetail,
  };
}

export default useDetail;
