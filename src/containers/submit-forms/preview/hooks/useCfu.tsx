import callAPI from "@helpers/networking";
import { IRadio } from "@interfaces/iradio";
import { useState } from "react";

import { SubmitPayload } from "@libraries/api";

interface IDataCfu {
  id: number;
  function_unit_id: number;
  code: string;
  name: string;
  created_at: string;
  updated_at: string;
  function_unit_name: string;
}

function makeDataCfu(data: IDataCfu[]): IRadio[] {
  return data.map((val: IDataCfu) => ({
    label: val.name,
    value: String(val.id),
  })) as IRadio[];
}

function useCfu() {
  const [loading, setLoading] = useState<boolean>(false);
  const [dataDivisi, setDataDivisi] = useState<IRadio[]>([] as IRadio[]);
  const [dataFu, setDataFu] = useState<IRadio[]>([] as IRadio[]);

  async function fetchCfuFunctionName() {
    setLoading(true);
    try {
      const response = await SubmitPayload.fetchCfuFunctionName();
      setDataDivisi(makeDataCfu(response.data.data as IDataCfu[]));
      setLoading(false);
    } catch (error) {
      setLoading(false);
      return error;
    }
  }

  async function fetchCfuByFunctionUnit(functionUnit: string) {
    setLoading(true);
    setDataFu([]);
    try {
      const response = await SubmitPayload.fetchCfuByFunctionName(functionUnit);
      setDataFu(makeDataCfu(response.data.data as IDataCfu[]));
      setLoading(false);
    } catch (error) {
      setLoading(false);
      return error;
    }
  }

  return {
    loading,
    dataDivisi,
    dataFu,
    fetchCfuFunctionName,
    fetchCfuByFunctionUnit,
  };
}

export default useCfu;
