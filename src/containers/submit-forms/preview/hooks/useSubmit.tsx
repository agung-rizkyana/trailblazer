import { useState } from "react";
import SubmitFormPayload from "@libraries/api/submit-form";
import { ISubmitForm } from "@interfaces/isubmit-form";
import { useMyModal } from "@components/modals/modal.context";

function useSubmit() {
  const { failed, close } = useMyModal();
  const [loading, setLoading] = useState<boolean>(false);
  async function update(data: ISubmitForm, id: string) {
    setLoading(true);
    try {
      const response = await SubmitFormPayload.update(data, id);
      return response;
    } catch (error) {
      failed({
        onOk: close,
      });
    } finally {
      setLoading(false);
    }
  }

  return {
    loading,
    update,
  };
}

export default useSubmit;
