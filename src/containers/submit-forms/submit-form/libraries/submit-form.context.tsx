import { ISubmitForm } from "@interfaces/isubmit-form";
import { createContext, useContext, useState } from "react";

export interface ISubmitFormContext {
  data: ISubmitForm;
  setData(data: ISubmitForm): void;
}

const initialValues: ISubmitFormContext = {
  data: {} as ISubmitForm,
  setData(data: ISubmitForm) { },
};

export const SubmitFormContext = createContext(initialValues);

export const useSubmitForm = () => {
  return useContext(SubmitFormContext);
};

export function withSubmitForm(WrapperComponent: React.FC) {
  const Content = (): JSX.Element => {
    const [data, setData] = useState<ISubmitForm>({} as ISubmitForm);
    return (
      <SubmitFormContext.Provider value={{
        data,
        setData,
      }}>
        <WrapperComponent />
      </SubmitFormContext.Provider>
    )
  };

  return Content;
}

export default withSubmitForm;
