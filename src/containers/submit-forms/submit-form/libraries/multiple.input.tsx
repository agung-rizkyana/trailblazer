import { Input, Button, Form, FormProps, Space } from "antd";

import { PlusOutlined, MinusOutlined } from "@ant-design/icons";

function MultipleInput(props: { name: string; itemName: string }) {
  return (
    <Form.List name={String(props.name)} initialValue={[""]}>
      {(fields, { add, remove }) => {
        return fields.map(({ key, name, fieldKey, ...restField }) => {
          return (
            <div style={{ display: "flex", justifyContent: "space-between" }}>
              <Form.Item
                {...restField}
                name={[name, String(props.itemName)]}
                fieldKey={[fieldKey, String(props.itemName)]}
                rules={[
                  { required: true, message: `Missing ${props.itemName}` },
                ]}
                style={{
                  flex: 1,
                  marginRight: ".5rem",
                }}
              >
                <Input size="large" placeholder="Jawaban bisa lebih dari 1" />
              </Form.Item>
              {fieldKey === 0 ? (
                <Button
                  type="primary"
                  size="large"
                  children={
                    <>
                      <PlusOutlined />
                    </>
                  }
                  onClick={() => add()}
                />
              ) : (
                <Button
                  type="ghost"
                  size="large"
                  children={
                    <>
                      <MinusOutlined />
                    </>
                  }
                  onClick={() => remove(name)}
                />
              )}
            </div>
          );
        });
      }}
    </Form.List>
  );
}

export default MultipleInput;
