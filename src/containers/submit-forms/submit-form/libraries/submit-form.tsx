import { IRadio } from "@interfaces/iradio";
import { ISubmitForm } from "@interfaces/isubmit-form";

export const DIVISI: IRadio[] = [
  { label: "Divisi Enterprise Service", value: "Divisi Enterprise Service" },
  { label: "Divisi Government Service", value: "Divisi Government Service" },
  { label: "Divisi Business Service", value: "Divisi Business Service" },
  {
    label: "Divisi Solution, Delivery & Assurance",
    value: "Divisi Solution, Delivery & Assurance",
  },
];

export const CFUFU: IRadio[] = [
  { label: "CFU Consumber", value: "CFU Consumber" },
  { label: "CFU Mobile", value: "CFU Mobile" },
  {
    label: "CFU Wholesale & International",
    value: "CFU Wholesale & International",
  },
  { label: "FU Digital Business", value: "FU Digital Business" },
  {
    label: "FU Finance & Risk Management",
    value: "FU Finance & Risk Management",
  },
  {
    label: "FU Human Capital Management",
    value: "FU Human Capital Management",
  },
  { label: "FU Network & IT Solution", value: "FU Network & IT Solution" },
  { label: "FU Strategic Portfolio", value: "FU Strategic Portfolio" },
];

export const REQUIREMENTS: IRadio[] = [
  { label: "online classroom", value: "online" },
  { label: "offline classroom", value: "offline" },
  { label: "mixed (online & offline classroom)", value: "mixed" },
];

export const LEARNING_STYLE: IRadio[] = [
  { label: "Presentation", value: "Presentation" },
  { label: "Evaluation", value: "Evaluation" },
  { label: "Pre-Test", value: "Pre-Test" },
  { label: "Discussion", value: "Discussion" },
  { label: "Pre-Class Video Learning", value: "Pre-Class Video Learning" },
  { label: "Post-Test", value: "Post-Test" },
  { label: "Case Study", value: "Case Study" },
  { label: "Pre-Class e-Learning", value: "Pre-Class e-Learning" },
  { label: "Practice / Lab", value: "Practice / Lab" },
];

export const SCHOOLS: IRadio[] = [
  { label: "Squad Learning Diagnostic", value: "Squad Learning Diagnostic" },
  {
    label: "School of Digital Connectivity",
    value: "School of Digital Connectivity",
  },
  {
    label: "School of Digital Go To Market",
    value: "School of Digital Go To Market",
  },
  {
    label: "School of Digital Platform & Digital Services",
    value: "School of Digital Platform & Digital Services",
  },
  { label: "School of Digital Enabler", value: "School of Digital Enabler" },
  {
    label: "School of Leadership & Transformation",
    value: "School of Leadership & Transformation",
  },
  {
    label: "School of Digital Telecommunication & Media",
    value: "School of Digital Telecommunication & Media",
  },
];

export const initialValues: ISubmitForm = {
  division: "CFU Enterprise",
  cfu_fu: "Divisi Business Service",
  whys: [
    {
      why: "Latar Belakang 1",
    },
  ],
  org_whats: [
    {
      organization_what: "Target Organisasi 1",
    },
  ],
  ind_whats: [
    {
      individual_what: "Target Individu 1",
    },
  ],
  ubis_participants: "> 40 Peserta",
  ubis_budget: "> Rp. 50.000.000",
  ubis_days: "> 7 Hari",
  ubis_other_info: "",
  individual_band: "III",
  individual_age: "35 - 40 Tahun",
  individual_unit: "DBS",
  individual_other_info: "",
  success: [
    {
      success_keys: "Kunci Sukses 1",
    },
  ],
  training_name: "Judul Pelatihan",
  training_date: "2021-07-30",
  strategic_plan: "Program Strategis Perusahaan",
  ubis_goal: "Tujuan Pelatihan Unit",
  individual_goal: "Tujuan Pelatihan Individu",
  skills: [
    {
      objective_skills: "Skill 1",
    },
  ],
  delivery_method: "mixed (online & offline classroom)",
  learning_styles: ["Presentation", "Evaluation"],
  unit_name: "School of Digital Connectivity", // akan diubah menjadi unit_id
  status: 1, // status : -1 {Draft}, 0 {Learning Analyst}, 1 {Design}, 2 {Development} ...
};

export const JUMLAH_PESERTA: IRadio[] = [
  { label: "10 orang", value: "10 orang" },
  { label: "20 orang", value: "20 orang" },
  { label: "> 50 orang", value: "> 50 orang" },
];

export const POSISI: IRadio[] = [
  { label: "BP. I", value: "BP. I" },
  { label: "BP. II", value: "BP. II" },
  { label: "BP. III", value: "BP. III" },
  { label: "BP. IV", value: "BP. IV" },
  { label: "BP. V", value: "BP. V" },
  { label: "BP. VI", value: "BP. VI" },
];

export const BUDGET: IRadio[] = [
  { label: "Rp. 50.000.000", value: "Rp. 50.000.000" },
  {
    label: "Rp. 50.000.000 - Rp. 100.000.000",
    value: "Rp. 50.000.000 - Rp. 100.000.000",
  },
  { label: "> Rp. 100.000.000", value: "> Rp. 100.000.000" },
];

export const USIA: IRadio[] = [
  { label: "20 - 35 tahun", value: "20 - 35 tahun" },
  { label: "35 - 50 tahun", value: "35 - 50 tahun" },
  { label: "> 50 Tahun", value: "> 50 Tahun" },
];

export const LAMA_PELAKSANAAN: IRadio[] = [
  { label: "< 7 hari", value: "< 7 hari" },
  { label: "7 - 14 hari", value: "7 - 14 hari" },
  { label: "> 14 hari", value: "> 14 hari" },
];

export const LOKASI_KERJA: IRadio[] = [
  { label: "Bandung", value: "Bandung" },
  { label: "Jakarta", value: "Jakarta" },
  { label: "Surabaya", value: "Surabaya" },
  { label: "Jogjakarta", value: "Jogjakarta" },
];
