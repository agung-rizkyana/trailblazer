import {
  LEARNING_STYLE,
  REQUIREMENTS,
  DIVISI,
  CFUFU,
  JUMLAH_PESERTA,
  POSISI,
  BUDGET,
  USIA,
  LAMA_PELAKSANAAN,
  LOKASI_KERJA,
} from "./libraries/submit-form";

import {
  Button,
  Layout,
  Row,
  Col,
  Form,
  Input,
  Radio,
  Checkbox,
  Typography,
  Select,
  DatePicker,
  Space,
} from "antd";
import { IRadio } from "@interfaces/iradio";

import Info from "@components/info";

import { withModal, useMyModal } from "@components/modals/modal.context";
import { useHistory } from "react-router-dom";
import MultipleInput from "./libraries/multiple.input";
import { ISubmitForm } from "@interfaces/isubmit-form";

import useSubmit from "./hooks/useSubmit";
import { useSubmitForm } from "./libraries/submit-form.context";

import { ModalSubmit, ModalConfirm } from "@components/modals";
import { useEffect, useState } from "react";
import useCfu from "./hooks/useCfu";
import { SelectValue } from "antd/lib/select";

import moment from "moment";

const { Content } = Layout;

function disabledDate(current: any) {
  // Can not select days before today and today
  return current && current < moment().endOf("day");
}

function SubmitForm() {
  const [showSubmit, setShowSubmit] = useState<boolean>(false);
  const [showConfirm, setShowConfirm] = useState<boolean>(false);
  const [school, setSchool] = useState<string>("");
  const [payload, setPayload] = useState<ISubmitForm>({} as ISubmitForm);
  const { setData } = useSubmitForm();
  const [form] = Form.useForm();

  form.setFields([
    { name: "why", value: [""] },
    { name: "organization_what", value: [""] },
    { name: "individual_what", value: [""] },
    { name: "success_keys", value: [""] },
    { name: "objective_skills", value: [""] },
    { name: "learning_styles", value: ["Presentation"] },
  ]);

  const { failed, succces, confirm, close, submit } = useMyModal();
  const { loading, doSubmit } = useSubmit();
  const { fetchCfuFunctionName, fetchCfuByFunctionUnit, dataDivisi, dataFu } =
    useCfu();

  // fetch divisi
  useEffect(() => {
    fetchCfuFunctionName();
  }, []);

  useEffect(() => {
    form.setFields([
      {
        name: "cfu_fu",
        value: null,
      },
    ]);
    console.log("masuk data divisi");
  }, [form.getFieldValue("division")]);

  const history = useHistory();

  const handleSubmit = async (values: ISubmitForm) => {
    // make payload
    const individual_what = values.ind_whats.map(
      (v: { individual_what: string }) => v.individual_what
    );
    const organization_what = values.org_whats.map(
      (v: { organization_what: string }) => v.organization_what
    );
    const objective_skills = values.skills.map(
      (v: { objective_skills: string }) => v.objective_skills
    );
    const success_keys = values.success.map(
      (v: { success_keys: string }) => v.success_keys
    );
    const why = values.whys.map((v: { why: string }) => v.why);

    // const training_date = values.training_date

    const makePayload = Object.assign(values, {
      individual_what,
      organization_what,
      objective_skills,
      success_keys,
      why,
      unit_name: "School of Digital Connectivity",
      status: -1,
      is_submitted: false,
    });

    const response = await doSubmit(makePayload);
    history.push(`/submit-form/preview/${response?.id}`);
  };

  const handleFailed = () => {
    failed({
      onOk: close,
    });
  };

  const makeSubmit = async () => {
    setShowConfirm(false);
    await doSubmit(payload);
    succces({
      onOk: close,
    });
  };

  return (
    <>
      <Typography children="Submit Form" className="title" />
      <Content className="content-page">
        <Form
          form={form}
          layout="vertical"
          onFinish={(values: ISubmitForm) => handleSubmit(values)}
          onFinishFailed={handleFailed}
        >
          {/* <ModalSubmit
            visible={showSubmit}
            onOk={() => handleSubmit(form.getFieldsValue())}
            onCancel={() => setShowSubmit(false)}
          /> */}
          <Row gutter={12}>
            <Col span={12}>
              <Form.Item
                label="CFU/FU"
                name="division_id"
                rules={[
                  {
                    min: 1,
                    required: true,
                  },
                ]}
              >
                <Select
                  size="large"
                  placeholder="Pilih divisi Anda saat ini"
                  onChange={(val: SelectValue) => {
                    fetchCfuByFunctionUnit(String(val));
                  }}
                >
                  {dataDivisi.map((d: IRadio, i: number) => (
                    <Select.Option key={i} value={d.value}>
                      {d.label}
                    </Select.Option>
                  ))}
                </Select>
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                label="Divisi"
                name="function_unit_id"
                rules={[
                  {
                    min: 1,
                    required: true,
                  },
                ]}
              >
                <Select size="large" placeholder="Pilih CFU/FU Anda saat ini">
                  {dataFu.map((c: IRadio, i: number) => (
                    <Select.Option key={i} value={c.value}>
                      {c.label}
                    </Select.Option>
                  ))}
                </Select>
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <Info
                label="WHY"
                tooltipText="Contoh: Gangguan berulang Indihome melebihi batas maks. 50%"
              />
            </Col>
            <Col span={24} className="tb-form-item">
              <Typography>
                Jelaskan <span className="text-bold">latar belakang</span>{" "}
                kebutuhan program pelatihan?
              </Typography>
              <MultipleInput name="whys" itemName="why" />
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <Info
                label="WHAT"
                tooltipText="Contoh: Meningkatkan penyelesaian gangguan berulang IndiHome menjadi 45%"
              />
            </Col>
            <Col span={24} className="tb-form-item">
              <Typography>
                Jelaskan <span className="text-bold">target</span> yang hendak
                dicapai pada level <span className="text-bold">organisasi</span>
                ?
              </Typography>
              <MultipleInput name="org_whats" itemName="organization_what" />
            </Col>
          </Row>
          <Row>
            <Col span={24} className="tb-form-item">
              <Typography>
                Jelaskan <span className="text-bold">target</span> yang hendak
                dicapai pada level <span className="text-bold">individu</span>?
              </Typography>
              <MultipleInput name="ind_whats" itemName="individual_what" />
            </Col>
          </Row>
          <Row gutter={12}>
            <Col span={24}>
              <Info
                label="Informasi lain yang dibutuhkan"
                tooltipText="Informasi lain yang perlu diketahui baik dari Unit Bisnis maupun Individu"
              />
            </Col>
            <Col span={12}>
              <Typography className="text-bold">Dari Unit Bisnis</Typography>
            </Col>
            <Col span={12}>
              <Typography className="text-bold">Dari Individu</Typography>
            </Col>
            <Col span={12}>
              <Form.Item
                rules={[
                  {
                    required: true,
                  },
                ]}
                name="ubis_participants"
                label="Jumlah Peserta"
              >
                <Select
                  size="large"
                  placeholder="Berapa orang yang ikut pelatihan"
                >
                  {JUMLAH_PESERTA.map((v: IRadio, i: number) => (
                    <Select.Option value={v.value}>{v.label}</Select.Option>
                  ))}
                </Select>
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                rules={[
                  {
                    required: true,
                  },
                ]}
                label="Posisi"
                name="individual_band"
              >
                <Select size="large" placeholder="Band posisi">
                  {POSISI.map((v: IRadio, i: number) => (
                    <Select.Option value={v.value}>{v.label}</Select.Option>
                  ))}
                </Select>
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                rules={[
                  {
                    required: true,
                  },
                ]}
                label="Budget"
                name="ubis_budget"
              >
                <Select size="large" placeholder="Rencana budget pelatihan">
                  {BUDGET.map((v: IRadio, i: number) => (
                    <Select.Option value={v.value}>{v.label}</Select.Option>
                  ))}
                </Select>
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                rules={[
                  {
                    required: true,
                  },
                ]}
                label="Rentang Usia"
                name="individual_age"
              >
                <Select size="large" placeholder="Umur peserta">
                  {USIA.map((v: IRadio, i: number) => (
                    <Select.Option value={v.value}>{v.label}</Select.Option>
                  ))}
                </Select>
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                rules={[
                  {
                    required: true,
                  },
                ]}
                label="Lama Pelaksanaan"
                name="ubis_days"
              >
                <Select
                  size="large"
                  placeholder="Berapa hari rencana pelatihan"
                >
                  {LAMA_PELAKSANAAN.map((v: IRadio, i: number) => (
                    <Select.Option value={v.value}>{v.label}</Select.Option>
                  ))}
                </Select>
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                rules={[
                  {
                    required: true,
                  },
                ]}
                label="Lokasi Kerja"
                name="individual_unit"
              >
                <Select size="large" placeholder="Unit Kerja saat ini">
                  {LOKASI_KERJA.map((v: IRadio, i: number) => (
                    <Select.Option value={v.value}>{v.label}</Select.Option>
                  ))}
                </Select>
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                rules={[
                  {
                    required: true,
                  },
                ]}
                label="Informasi lainnya"
                name="ubis_other_info"
              >
                <Input size="large" placeholder="e.g xxxx xxxxx xxxx" />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                rules={[
                  {
                    required: true,
                  },
                ]}
                label="Informasi lainnya"
                name="individual_other_info"
              >
                <Input size="large" placeholder="e.g xxxx xxxxx xxxx" />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={12}>
            <Col span={24}>
              <Info
                label="USULAN PROGRAM PELATIHAN"
                tooltipText={
                  <Space align="start" direction="vertical">
                    <Typography>
                      Judul pelatihan bisa berubah sesuai dengan hasil
                      klarifikasi lebih lanjut
                    </Typography>
                    <Typography className="text-bold">Contoh</Typography>
                    <div>
                      <Typography className="text-bold">
                        Kunci Sukses :{" "}
                      </Typography>
                      <Typography>
                        Peningkatan knowledge dalam penanganan gangguan Indihome
                      </Typography>
                    </div>
                    <div>
                      <Typography className="text-bold">
                        Program Strategis :{" "}
                      </Typography>
                      <Typography>
                        Peningkatan KPI Laten Divisi Consumer
                      </Typography>
                    </div>
                  </Space>
                }
              />
            </Col>
            <Col span={24} className="tb-form-item">
              <Typography>
                Indikator apa saja yang menjadi{" "}
                <span className="text-bold">kunci sukses</span> program
                pelatihan?
              </Typography>
              <MultipleInput name="success" itemName="success_keys" />
            </Col>
          </Row>
          <Row gutter={12}>
            <Col span={12}>
              <Form.Item
                rules={[
                  {
                    required: true,
                  },
                ]}
                name="training_name"
                label="Nama Pelatihan"
              >
                <Input size="large" placeholder="e.g xxxx xxxxx xxxx" />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                rules={[
                  {
                    required: true,
                  },
                ]}
                name="training_date"
                label="Tanggal Pelatihan"
              >
                <DatePicker
                  size="large"
                  placeholder="Pilih tanggal pelatihan"
                  className="input-datepicker"
                  disabledDate={disabledDate}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <Form.Item
                name="strategic_plan"
                rules={[
                  {
                    required: true,
                  },
                ]}
                label={
                  <Typography>
                    Jelaskan{" "}
                    <span className="text-bold">
                      program strategis perusahaan
                    </span>{" "}
                    yang akan didukung oleh program pelatihan ini?
                  </Typography>
                }
              >
                <Input.TextArea
                  size="large"
                  placeholder="e.g xxxx xxxxx xxxx"
                />
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <Info
                label="GOALS"
                tooltipText={
                  <Space direction="vertical" align="start">
                    <Typography className="text-bold">Contoh</Typography>
                    <div>
                      <Typography className="text-bold">
                        Tujuan pelatihan Unit :{" "}
                      </Typography>
                      <Typography>
                        Peserta berkontribusi sebesar 50% dalam pemenangan
                        proyek di enterprise dengan implementasi kemampuan
                        presentasi selama 3 hari kelas online dan 2 bulan masa
                        pemantauan
                      </Typography>
                    </div>
                    <div>
                      <Typography className="text-bold">
                        Tujuan pelatihan individu :{" "}
                      </Typography>
                      <Typography>
                        Peserta mendemonstrasikan kemampuan dalam presentasi
                        proyek Enterprise kepada klien
                      </Typography>
                    </div>
                    <div>
                      <Typography>
                        <span className="text-bold">Skill : </span> Peserta
                        diharapkan mampu mengimplementasikan kemampuan
                        presentasi dengan baik
                      </Typography>
                    </div>
                  </Space>
                }
              />
            </Col>
            <Col span={24}>
              <Form.Item
                name="ubis_goal"
                rules={[
                  {
                    required: true,
                  },
                ]}
                label={
                  <Typography>
                    Jelaskan <span className="text-bold">tujuan pelatihan</span>{" "}
                    yang ingin dicapai terhadap{" "}
                    <span className="text-bold">unit</span>?
                  </Typography>
                }
              >
                <Input.TextArea
                  size="large"
                  placeholder="e.g xxxx xxxxx xxxx"
                />
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <Form.Item
                name="individual_goal"
                rules={[
                  {
                    required: true,
                  },
                ]}
                label={
                  <Typography>
                    Jelaskan <span className="text-bold">tujuan pelatihan</span>{" "}
                    yang ingin dicapai terhadap{" "}
                    <span className="text-bold">individu</span>?
                  </Typography>
                }
              >
                <Input.TextArea
                  size="large"
                  placeholder="e.g xxxx xxxxx xxxx"
                />
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={24} className="tb-form-item">
              <Typography>
                Jelaskan apa saja{" "}
                <span className="text-bold">skill yang ingin dikuasai</span>{" "}
                oleh peserta setelah mengikuti pelatihan ini?
              </Typography>

              <MultipleInput name="skills" itemName="objective_skills" />
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <Info
                label="REQUIREMENT"
                tooltipText="Modul apa saja yang dibutuhkan dalam pelatihan"
              />
            </Col>
            <Col span={24}>
              <Form.Item
                name="delivery_method"
                rules={[
                  {
                    required: true,
                  },
                ]}
                label={
                  <Typography>
                    Jelaskan{" "}
                    <span className="text-bold">
                      kebutuhan materi pelatihan?
                    </span>
                  </Typography>
                }
              >
                <Radio.Group>
                  <Space direction="vertical">
                    {REQUIREMENTS.map((r: IRadio, i: number) => (
                      <Radio value={r.value} key={i}>
                        {r.label}
                      </Radio>
                    ))}
                  </Space>
                </Radio.Group>
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <Form.Item
                name="learning_styles"
                rules={[
                  {
                    required: true,
                  },
                ]}
                label={
                  <Typography>
                    <span className="text-bold">Learning style</span> yang
                    dibutuhkan pada program pelatihan?
                  </Typography>
                }
              >
                <Checkbox.Group>
                  <Row>
                    {LEARNING_STYLE.map((l: IRadio, i: number) => (
                      <Col span={8} key={i}>
                        <Checkbox
                          value={l.value}
                          style={{ lineHeight: "32px" }}
                        >
                          {l.label}
                        </Checkbox>
                      </Col>
                    ))}
                  </Row>
                </Checkbox.Group>
                <Typography className="text-danger helper-text">
                  *Choose as many as you like
                </Typography>
              </Form.Item>
            </Col>
          </Row>
          <Row style={{ marginTop: "3rem" }}>
            <Col span={24} className="text-center">
              <Button
                loading={loading}
                htmlType="submit"
                size="large"
                type="ghost"
                children="Save as Draft"
                style={{ marginRight: "15px" }}
              />
              <Button
                htmlType="submit"
                size="large"
                type="primary"
                children="Preview"
                className="btn-preview"
              />
            </Col>
          </Row>
        </Form>
      </Content>
      {/* <ModalConfirm
        visible={showConfirm && Boolean(payload.unit_name)}
        school={payload.unit_name}
        onCancel={() => setShowConfirm(false)}
        onOk={() => makeSubmit()}
      /> */}
    </>
  );
}

export default withModal(SubmitForm);
