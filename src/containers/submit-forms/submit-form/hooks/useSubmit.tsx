import { useState } from "react";
import SubmitFormPayload from "@libraries/api/submit-form";
import { ISubmitForm } from "@interfaces/isubmit-form";
import { useMyModal } from "@components/modals/modal.context";

function useSubmit() {
  const { failed, close } = useMyModal();
  const [loading, setLoading] = useState<boolean>(false);
  async function doSubmit(data: ISubmitForm) {
    setLoading(true);
    try {
      const response = await SubmitFormPayload.submit(data);
      return response.data.data;
    } catch (error) {
      failed({
        onOk: close,
      });
    } finally {
      setLoading(false);
    }
  }

  return {
    loading,
    doSubmit,
  };
}

export default useSubmit;
