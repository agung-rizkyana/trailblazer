import React, { useContext, useEffect } from "react";

import {
  LEARNING_STYLE,
  REQUIREMENTS,
  DIVISI,
  CFUFU,
  SAMPLE_PREVIEW,
} from "./libraries/preview";

import { PlusOutlined, InfoOutlined } from "@ant-design/icons";

import {
  Button,
  Layout,
  Row,
  Col,
  Form,
  Input,
  Radio,
  Checkbox,
  Typography,
  Select,
  DatePicker,
  Space,
} from "antd";
import { IRadio } from "@interfaces/iradio";

import Info from "@components/info";

import { Save, Delete, Download, Edit } from "@components/icons";

import { withModal, useMyModal } from "@components/modals/modal.context";
import {
  useSubmitForm,
  withSubmitForm,
} from "@containers/submit-forms/submit-form";
import usePreview from "./hooks/usePreview";
import { useHistory, useParams } from "react-router-dom";

const { Content } = Layout;

function PreviewForm() {
  const history = useHistory();
  const { id } = useParams() as { id: string };
  // const { data } = useSubmitForm();
  const [form] = Form.useForm();
  const { submit, close, confirm, succces } = useMyModal();

  const { loading, fetchDetail, data } = usePreview();

  useEffect(() => {
    fetchDetail(id);
  }, [id]);

  useEffect(() => {
    form.setFields([
      { name: "delivery_method", value: data.delivery_method },
      { name: "learning_styles", value: data.learning_styles },
    ]);
  }, [data]);

  return data ? (
    <>
      <Row gutter={12} align="middle" style={{ marginBottom: "1rem" }}>
        <Col span={8}>
          <Typography children="Preview" className="title" />
        </Col>
        <Col span={16}>
          <Row gutter={12} align="middle">
            <Col span={16} className="text-right">
              <Button
                type="link"
                children={
                  <>
                    <Save />
                    <Typography className="text-danger" children="Save" />
                  </>
                }
              />
              <Button
                type="link"
                children={
                  <>
                    <Delete />
                    <Typography className="text-danger" children="Delete" />
                  </>
                }
              />
              <Button
                type="link"
                children={
                  <>
                    <Download />
                    <Typography className="text-danger" children="Download" />
                  </>
                }
              />
              <Button
                type="link"
                children={
                  <>
                    <Edit />
                    <Typography className="text-danger" children="Edit" />
                  </>
                }
              />
            </Col>
            <Col span={8}>
              <Button
                block
                size="large"
                type="primary"
                children="Submit"
                className="btn-success"
                onClick={() =>
                  submit({
                    onOk: () => {
                      confirm({
                        onOk: () => {
                          succces({
                            onOk: () => {
                              history.goBack();
                            },
                          });
                        },
                        onCancel: close,
                      });
                    },
                  })
                }
              />
            </Col>
          </Row>
        </Col>
      </Row>
      <Content className="content-page">
        <Form form={form} layout="vertical">
          <Row gutter={12}>
            <Col span={12}>
              <Form.Item label="Divisi">
                <Typography children={data.division} />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item label="CFU/FU">
                <Typography children={data.cfu_fu} />
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <Info
                label="WHY"
                tooltipText="Contoh: Gangguan berulang Indihome melebihi batas maks. 50%"
              />
            </Col>
            <Col span={24}>
              <Form.Item
                label={
                  <Typography>
                    Jelaskan <span className="text-bold">latar belakang</span>{" "}
                    kebutuhan program pelatihan?
                  </Typography>
                }
                className="tb-form-item"
              >
                <Typography children={data.why} />
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <Info
                label="WHAT"
                tooltipText="Contoh: Meningkatkan penyelesaian gangguan berulang IndiHome menjadi 45%"
              />
            </Col>
            <Col span={24}>
              <Form.Item
                label={
                  <Typography>
                    Jelaskan <span className="text-bold">target</span> yang
                    hendak dicapai pada level{" "}
                    <span className="text-bold">organisasi</span>?
                  </Typography>
                }
                className="tb-form-item"
              >
                <Typography children={data.organization_what} />
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <Form.Item
                label={
                  <Typography>
                    Jelaskan <span className="text-bold">target</span> yang
                    hendak dicapai pada level{" "}
                    <span className="text-bold">individu</span>?
                  </Typography>
                }
                className="tb-form-item"
              >
                <Typography children={data.individual_what} />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={12}>
            <Col span={24}>
              <Info
                label="Informasi lain yang dibutuhkan"
                tooltipText="Informasi lain yang perlu diketahui baik dari Unit Bisnis maupun Individu"
              />
            </Col>
            <Col span={12}>
              <Typography className="text-bold">Dari Unit Bisnis</Typography>
            </Col>
            <Col span={12}>
              <Typography className="text-bold">Dari Individu</Typography>
            </Col>
            <Col span={12}>
              <Form.Item label="Jumlah Peserta">
                <Typography children={data.ubis_participants} />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item label="Posisi">
                <Typography children={data.individual_band} />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item label="Budget">
                <Typography children={data.ubis_budget} />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item label="Rentang Usia">
                <Typography children={data.individual_age} />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item label="Lama Pelaksanaan">
                <Typography children={data.ubis_days} />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item label="Lokasi Kerja">
                <Typography children={data.individual_unit} />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item label="Informasi lainnya">
                <Typography children={data.ubis_other_info} />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item label="Informasi lainnya">
                <Typography children={data.individual_other_info} />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={12}>
            <Col span={24}>
              <Info
                label="USULAN PROGRAM PELATIHAN"
                tooltipText={
                  <Space align="start" direction="vertical">
                    <Typography>
                      Judul pelatihan bisa berubah sesuai dengan hasil
                      klarifikasi lebih lanjut
                    </Typography>
                    <Typography className="text-bold">Contoh</Typography>
                    <div>
                      <Typography className="text-bold">
                        Kunci Sukses :{" "}
                      </Typography>
                      <Typography>
                        Peningkatan knowledge dalam penanganan gangguan Indihome
                      </Typography>
                    </div>
                    <div>
                      <Typography className="text-bold">
                        Program Strategis :{" "}
                      </Typography>
                      <Typography>
                        Peningkatan KPI Laten Divisi Consumer
                      </Typography>
                    </div>
                  </Space>
                }
              />
            </Col>
            <Col span={24}>
              <Form.Item
                label={
                  <Typography>
                    Indikator apa saja yang menjadi{" "}
                    <span className="text-bold">kunci sukses</span> program
                    pelatihan?
                  </Typography>
                }
                className="tb-form-item"
              >
                <Typography children={data.success_keys} />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={12}>
            <Col span={12}>
              <Form.Item label="Nama Pelatihan">
                <Typography children={data.training_name} />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item label="Tanggal Pelatihan">
                <Typography children={data.training_date} />
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <Form.Item
                label={
                  <Typography>
                    Jelaskan{" "}
                    <span className="text-bold">
                      program strategis perusahaan
                    </span>{" "}
                    yang akan didukung oleh program pelatihan ini?
                  </Typography>
                }
              >
                <Typography children={data.strategic_plan} />
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <Info
                label="GOALS"
                tooltipText={
                  <Space direction="vertical" align="start">
                    <Typography className="text-bold">Contoh</Typography>
                    <div>
                      <Typography className="text-bold">
                        Tujuan pelatihan Unit :{" "}
                      </Typography>
                      <Typography>
                        Peserta berkontribusi sebesar 50% dalam pemenangan
                        proyek di enterprise dengan implementasi kemampuan
                        presentasi selama 3 hari kelas online dan 2 bulan masa
                        pemantauan
                      </Typography>
                    </div>
                    <div>
                      <Typography className="text-bold">
                        Tujuan pelatihan individu :{" "}
                      </Typography>
                      <Typography>
                        Peserta mendemonstrasikan kemampuan dalam presentasi
                        proyek Enterprise kepada klien
                      </Typography>
                    </div>
                    <div>
                      <Typography>
                        <span className="text-bold">Skill : </span> Peserta
                        diharapkan mampu mengimplementasikan kemampuan
                        presentasi dengan baik
                      </Typography>
                    </div>
                  </Space>
                }
              />
            </Col>
            <Col span={24}>
              <Form.Item
                label={
                  <Typography>
                    Jelaskan <span className="text-bold">tujuan pelatihan</span>{" "}
                    yang ingin dicapai terhadap{" "}
                    <span className="text-bold">unit</span>?
                  </Typography>
                }
              >
                <Typography children={data.ubis_goal} />
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <Form.Item
                label={
                  <Typography>
                    Jelaskan <span className="text-bold">tujuan pelatihan</span>{" "}
                    yang ingin dicapai terhadap{" "}
                    <span className="text-bold">individu</span>?
                  </Typography>
                }
              >
                <Typography children={data.individual_goal} />
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <Form.Item
                label={
                  <Typography>
                    Jelaskan apa saja{" "}
                    <span className="text-bold">skill yang ingin dikuasai</span>{" "}
                    oleh peserta setelah mengikuti pelatihan ini?
                  </Typography>
                }
                className="tb-form-item"
              >
                <Typography children={data.skills} />
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <Info
                label="REQUIREMENT"
                tooltipText="Modul apa saja yang dibutuhkan dalam pelatihan"
              />
            </Col>
            <Col span={24}>
              <Form.Item
                name="delivery_method"
                label={
                  <Typography>
                    Jelaskan{" "}
                    <span className="text-bold">
                      kebutuhan materi pelatihan?
                    </span>
                  </Typography>
                }
              >
                <Radio.Group defaultValue={data.delivery_method}>
                  <Space direction="vertical">
                    {REQUIREMENTS.map((r: IRadio, i: number) => (
                      <Radio value={r.value} key={i}>
                        {r.label}
                      </Radio>
                    ))}
                  </Space>
                </Radio.Group>
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <Form.Item
                name="checkbox-group"
                label={
                  <Typography>
                    <span className="text-bold">Learning style</span> yang
                    dibutuhkan pada program pelatihan?
                  </Typography>
                }
              >
                <Checkbox.Group defaultValue={data.learning_styles}>
                  <Row>
                    {LEARNING_STYLE.map((l: IRadio, i: number) => (
                      <Col span={8} key={i}>
                        <Checkbox
                          value={l.value}
                          style={{ lineHeight: "32px" }}
                        >
                          {l.label}
                        </Checkbox>
                      </Col>
                    ))}
                  </Row>
                </Checkbox.Group>
                <Typography className="text-danger helper-text">
                  *Choose as many as you like
                </Typography>
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </Content>
    </>
  ) : (
    <>Loading...</>
  );
}

const PreviewFormContainer = withModal(PreviewForm);

export default withSubmitForm(PreviewFormContainer);
