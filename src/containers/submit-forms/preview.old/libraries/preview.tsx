import { IRadio } from '@interfaces/iradio';

export const DIVISI: IRadio[] = [
  { label: 'CFU Consumber', value: 'CFU Consumber' },
  { label: 'CFU Mobile', value: 'CFU Mobile' },
  { label: 'CFU Wholesale & International', value: 'CFU Wholesale & International' },
  { label: 'FU Digital Business', value: 'FU Digital Business' },
  { label: 'FU Finance & Risk Management', value: 'FU Finance & Risk Management' },
  { label: 'FU Human Capital Management', value: 'FU Human Capital Management' },
  { label: 'FU Network & IT Solution', value: 'FU Network & IT Solution' },
  { label: 'FU Strategic Portfolio', value: 'FU Strategic Portfolio' },
];

export const CFUFU: IRadio[] = [
  { label: 'Divisi Enterprise Service', value: 'Divisi Enterprise Service' },
  { label: 'Divisi Government Service', value: 'Divisi Government Service' },
  { label: 'Divisi Business Service', value: 'Divisi Business Service' },
  { label: 'Divisi Solution, Delivery & Assurance', value: 'Divisi Solution, Delivery & Assurance' },
];

export const REQUIREMENTS: IRadio[] = [
  { label: 'online classroom', value: 'online' },
  { label: 'offline classroom', value: 'offline' },
  { label: 'mixed (online & offline classroom)', value: 'mixed' },
];

export const LEARNING_STYLE: IRadio[] = [
  { label: 'Presentation', value: 'Presentation' },
  { label: 'Evaluation', value: 'Evaluation' },
  { label: 'Pre-Test', value: 'Pre-Test' },
  { label: 'Discussion', value: 'Discussion' },
  { label: 'Pre-Class Video Learning', value: 'Pre-Class Video Learning' },
  { label: 'Post-Test', value: 'Post-Test' },
  { label: 'Case Study', value: 'Case Study' },
  { label: 'Pre-Class e-Learning', value: 'Pre-Class e-Learning' },
  { label: 'Practice / Lab', value: 'Practice / Lab' },
];

export const SCHOOLS: IRadio[] = [
  { label: 'Squad Learning Diagnostic', value: 'Squad Learning Diagnostic' },
  { label: 'School of Digital Connectivity', value: 'School of Digital Connectivity' },
  { label: 'School of Digital Go To Market', value: 'School of Digital Go To Market' },
  { label: 'School of Digital Platform & Digital Services', value: 'School of Digital Platform & Digital Services' },
  { label: 'School of Digital Enabler', value: 'School of Digital Enabler' },
  { label: 'School of Leadership & Transformation', value: 'School of Leadership & Transformation' },
  { label: 'School of Digital Telecommunication & Media', value: 'School of Digital Telecommunication & Media' },
];

export const SAMPLE_PREVIEW = {
  divisi: 'CFU Enterprise',
  cfufu: 'Divisi Business Service',
  latarBelakang: 'Gangguan berulang Astinet melebihi batas maks. 50%',
  targetOrganisasi: 'Meningkatkan penyelesaian gangguan berulang Astinet menjadi 45%',
  targetIndividu: 'Meningkatkan kemampuan pengawalan assurance',
  unitBisnis: {
    jumlahPeserta: '10 orang',
    budget: 'Rp. 2.000.000,-',
    lamaPelaksanaan: '5 hari',
    informasiLainnya: 'Saran Lokasi TCU Area 2',
  },
  individu: {
    posisi: 'BP. V',
    rentangUsia: '25 - 35 tahun',
    lokasiKerja: 'Bandung',
    informasiLainnya: 'Tidak ada',
  },
  kunciSukses: 'Peningkatan MTTR sebesar 20%',
  namaPelatihan: 'Penanganan Gangguan Astinet',
  tanggalPelatihan: '2021-06-13',
  programStrategisPerusahaan: 'Senior Leader memberikan fire briefing dan memastikan pelatihan sangat diperlukan',
  tujuanUnit: 'Untuk meningkatkan keberhasilan penanganan gangguan pada corporate customer',
  tujuanIndividu: 'Peserta berkontribusi sebesar 50% dalam pemenangan proyek di enterprise dengan implementasi kemampuan presentasi selama 3 hari kelas online dan 2 bulan masa pemantauan',
  skill: 'Peserta mendemonstrasikan kemampuan dalam presentasi proyek Enterprise kepada klien',
  requirement: 'mixed',
  learnignStyle: 'presentation'

}