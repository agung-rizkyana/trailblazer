import { ISubmitForm } from "@interfaces/isubmit-form";
import { useState } from "react";
import PreviewPayload from '@libraries/api/preview';
import { useMyModal } from "@components/modals/modal.context";

function usePreview() {
  const { failed, close } = useMyModal();
  const [loading, setLoading] = useState<boolean>(false);
  const [data, setData] = useState<ISubmitForm>({} as ISubmitForm);
  async function fetchDetail(id: string) {
    setLoading(true);
    try {
      const response = await PreviewPayload.fetchDetail(id);
      setData(response.data.data);
    } catch (error) {
      failed({
        onOk: close,
      });
    } finally {
      setLoading(false);
    }
  }

  return {
    loading,
    fetchDetail,
    data,
  }
}

export default usePreview;
