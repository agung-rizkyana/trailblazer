import React from 'react';
import Container from '@containers/auth/login';
import { withAuthPage } from '@components/layouts/auth';

import { Helmet } from 'react-helmet';

function LoginPage() {
  return (
    <>
      <Helmet title="iLaborate - Login" />
      <Container />
    </>

  )
}

export default withAuthPage(LoginPage);
