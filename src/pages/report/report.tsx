import React from "react";
import { ReportList } from "@containers/report";
import { withAdminPage } from "@components/layouts/admin";

import { Helmet } from "react-helmet";

function ReportPage() {
  return (
    <>
      <Helmet title="iLaborate - Report" />
      <ReportList />
    </>
  );
}

const Page = withAdminPage(ReportPage, {
  breadcrumbs: ["Home", "Report"],
});

export default Page;
