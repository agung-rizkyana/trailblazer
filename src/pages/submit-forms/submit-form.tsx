import React from "react";
import Container from "@containers/submit-forms/submit-form";
import { withAdminPage } from "@components/layouts/admin";

import { Helmet } from "react-helmet";

function SubmitFormPage() {
  return (
    <>
      <Helmet title="iLaborate - Submit Form" />
      <Container />
    </>
  );
}

const Page = withAdminPage(SubmitFormPage, {
  breadcrumbs: ["Home", "Submit Form"],
});

export default Page;
