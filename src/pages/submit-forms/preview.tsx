import React from "react";
import Container from "@containers/submit-forms/preview";
import { withAdminPage } from "@components/layouts/admin";

import { Helmet } from "react-helmet";

function Preview() {
  return (
    <>
      <Helmet title="iLaborate - Preview" />
      <Container />
    </>
  );
}

const Page = withAdminPage(Preview, {
  breadcrumbs: ["Home", "Submit Form"],
});

export default Page;
