import { BASE_URL } from "@config/api";
import { AxiosRequestConfig } from "axios";

import AuthHelper from "@helpers/auth";
import callAPI from "@helpers/networking";

class Report {
  async fetchReport() {
    const payload: AxiosRequestConfig = {
      baseURL: BASE_URL,
      url: "submitted_forms/paging?page=1&per=10&sort=updated_at&direction=asc",
      headers: {
        "Content-Type": "application/json",
        Authorization: AuthHelper.getToken(),
      },
    };

    return await callAPI(payload);
  }
}

export default new Report();
