import { BASE_URL } from '@config/api';
import { ILogin } from '@containers/auth/login/libraries/login';
import callAPI from '@helpers/networking';
import { AxiosRequestConfig } from 'axios';

class AuthPayload {
  async login(data: ILogin) {
    const payload: AxiosRequestConfig = {
      baseURL: BASE_URL,
      url: 'login',
      method: 'post',
      headers: {
        'Content-Type': 'application/json',
      },
      data: {
        user: data,
      }
    }
    return await callAPI(payload);
  }
}

export default new AuthPayload();
