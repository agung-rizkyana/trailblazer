import { BASE_URL } from "@config/api";
// import { ILogin } from '@containers/auth/login/libraries/login';
import callAPI from "@helpers/networking";
import { ISubmitForm } from "@interfaces/isubmit-form";
import { AxiosRequestConfig } from "axios";

import AuthHelper from "@helpers/auth";
// import FormData from "form-data";

class SubmitFormPayload {
  async submit(data: ISubmitForm) {
    const payload: AxiosRequestConfig = {
      baseURL: BASE_URL,
      url: "submitted_forms",
      method: "post",
      headers: {
        "Content-Type": "application/json",
        Authorization: AuthHelper.getToken(),
      },
      data,
    };
    return await callAPI(payload);
  }
  async update(data: ISubmitForm, id: string) {
    const payload: AxiosRequestConfig = {
      baseURL: BASE_URL,
      url: `submitted_forms/${id}`,
      method: "put",
      headers: {
        "Content-Type": "application/json",
        Authorization: AuthHelper.getToken(),
      },
      data,
    };
    return await callAPI(payload);
  }
  async download(id: string, req_file: "pdf" | "xls" | "jpg") {
    const payload: AxiosRequestConfig = {
      baseURL: BASE_URL,
      url: `submitted_forms/download/${id}?req_file=${req_file}`,
      method: "get",
      headers: {
        "Content-Type": "multipart/form-data",
        Authorization: AuthHelper.getToken(),
        Accept: "*/*",
      },
      responseType: "blob",
    };
    return await callAPI(payload);
  }
  async fetchCfu() {
    const payload: AxiosRequestConfig = {
      baseURL: BASE_URL,
      url: "cfu_fus",
      method: "get",
      headers: {
        "Content-Type": "application/json",
        Authorization: AuthHelper.getToken(),
      },
    };
    return await callAPI(payload);
  }
  async fetchCfuByFunctionName(functionUnit: string) {
    const payload: AxiosRequestConfig = {
      baseURL: BASE_URL,
      url: `cfu_fus/${functionUnit}`,
      method: "get",
      headers: {
        "Content-Type": "application/json",
        Authorization: AuthHelper.getToken(),
      },
    };
    return await callAPI(payload);
  }

  async fetchCfuFunctionName() {
    const payload: AxiosRequestConfig = {
      baseURL: BASE_URL,
      url: "cfu_fus/function_unit_only",
      method: "get",
      headers: {
        "Content-Type": "application/json",
        Authorization: AuthHelper.getToken(),
      },
    };
    return await callAPI(payload);
  }

  async detail(id: string) {
    const payload: AxiosRequestConfig = {
      baseURL: BASE_URL,
      url: `submitted_forms/${id}`,
      method: "get",
      headers: {
        "Content-Type": "application/json",
        Authorization: AuthHelper.getToken(),
      },
    };
    return await callAPI(payload);
  }
}

export default new SubmitFormPayload();
