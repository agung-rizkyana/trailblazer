import { BASE_URL } from '@config/api';
// import { ILogin } from '@containers/auth/login/libraries/login';
import callAPI from '@helpers/networking';
import { ISubmitForm } from '@interfaces/isubmit-form';
import { AxiosRequestConfig } from 'axios';

import AuthHelper from '@helpers/auth';

class PreviewPayload {
  async fetchDetail(id: string) {
    const payload: AxiosRequestConfig = {
      baseURL: BASE_URL,
      url: `submitted_forms/${id}`,
      method: 'get',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': AuthHelper.getToken(),
      },
    }
    return await callAPI(payload);
  }
}

export default new PreviewPayload();
