import React, { createContext, useContext, useState } from "react";

import {
  ModalFailed,
  ModalConfirm,
  ModalSubmit,
  ModalSuccess,
} from '@components/modals';

export type TModal = 'success' | 'failed' | 'confirm' | 'submit';

export interface IModal {
  isOpen: boolean;
  modalType: TModal;
  closeModal?: () => void;
}

export interface IModalContextValue {
  modal: IModal;
  setModal(modal: IModal): void;
  onSubmit?: (values: any) => void;
  onConfirm?: () => void;
  onRequestClose?: () => void;
}

const initialValue: IModal = {
  isOpen: false,
  modalType: 'success',
  closeModal() { }
};

const contextValue: IModalContextValue = {
  modal: initialValue,
  setModal(modal: IModal) { },
}

export const ModalContext = createContext(contextValue);
export const useModal = () => {
  return useContext(ModalContext);
}

function withModal(WrappedComponent: React.FC) {
  const Content = (): JSX.Element => {
    const [modal, setModal] = useState<IModal>(initialValue);

    return (
      <ModalContext.Provider value={{
        modal,
        setModal,
      }}>
        <WrappedComponent />
        <ModalFailed visible={modal.isOpen && modal.modalType === 'failed'} />
        <ModalSubmit visible={modal.isOpen && modal.modalType === 'submit'} />
        <ModalConfirm visible={modal.isOpen && modal.modalType === 'confirm'} />
        <ModalSuccess visible={modal.isOpen && modal.modalType === 'success'} />
      </ModalContext.Provider>
    )
  };

  return Content;
}

export default withModal;
