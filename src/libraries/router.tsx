import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import { DASHBOARD, AUTH } from "@config/pages";
import { IPage, IPages } from "@interfaces/ipages";

function DashboardRoutes(): JSX.Element[] {
  const routes: any[] = [];
  DASHBOARD.forEach((route: IPages) => {
    const pages = route.pages;
    console.log("pages :: ", pages);

    pages.forEach((page: IPage, key: number) => {
      routes.push(
        <Route exact key={key} path={page.path} component={page.component} />
      );
    });
  });
  return routes;
}

function AuthRoutes(): JSX.Element[] {
  const routes: any[] = [];
  AUTH.forEach((route: IPages) => {
    const pages = route.pages;
    pages.forEach((page: IPage, key: number) => {
      routes.push(
        <Route exact key={key} path={page.path} component={page.component} />
      );
    });
  });
  return routes;
}

export function AppRoutes() {
  return (
    <Router>
      <Switch>
        {DashboardRoutes()}
        {AuthRoutes()}
      </Switch>
    </Router>
  );
}
