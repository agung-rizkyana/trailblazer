import React from "react";
import { AppRoutes } from "@libraries/router";

import "./App.less";

function App() {
  return AppRoutes();
}

export default App;
