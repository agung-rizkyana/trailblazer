import { IPages } from "@interfaces/ipages";

import { AuthLogin, Preview, SubmitForm, Report } from "@pages/index";

export const DASHBOARD: IPages[] = [
  {
    group: "Submit Form",
    pages: [
      {
        path: "/submit-form",
        component: SubmitForm,
      },
      {
        path: "/submit-form/preview/:id",
        component: Preview,
      },
    ],
  },
  {
    group: "Report",
    pages: [
      {
        path: "/report",
        component: Report,
      },
    ],
  },
];

export const AUTH: IPages[] = [
  {
    group: "Auth",
    pages: [
      {
        path: "/",
        component: AuthLogin,
      },
      {
        path: "/login",
        component: AuthLogin,
      },
    ],
  },
];
