class AuthHelper {
  private static instance: AuthHelper = new AuthHelper();
  private constructor() { }

  public static getInstance() {
    return this.instance;
  }

  public setToken(token: string) {
    localStorage.setItem('authToken', token);
  }

  public getToken() {
    return localStorage.getItem('authToken');
  }

  public clearToken() {
    localStorage.removeItem('authToken');
  }

}

export default AuthHelper.getInstance();