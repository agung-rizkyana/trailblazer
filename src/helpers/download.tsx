import FileDownload from "js-file-download";

export const directDownload = (data: any, fileName: string) => {
  FileDownload(data, fileName);
};
