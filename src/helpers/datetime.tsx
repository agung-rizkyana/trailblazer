import moment from "moment";

export function formatDate(stringDate: string) {
  return moment(stringDate).format("DD/MM/YYYY");
}
