import { IResponse } from "@interfaces/iresponse";
import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';

async function callAPI(payload: AxiosRequestConfig): Promise<AxiosResponse> {
  try {
    const response = await axios(payload);
    return response;
  } catch (error) {
    return Promise.reject(error);
  }
}

export default callAPI;
