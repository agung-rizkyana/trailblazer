import {
  Typography,
  Tooltip,
  Space,
} from 'antd';
import {
  InfoOutlined,
} from '@ant-design/icons';
import React, { PropsWithChildren } from 'react';

interface IInfoProps {
  label: string;
  tooltipText: string | React.ReactNode;
}

function Info({ label, tooltipText }: PropsWithChildren<IInfoProps>) {
  const ContentTooltip = () => {
    return (
      <Space align="center" direction="horizontal" style={{ width: '100%', overflow: 'hidden' }}>
        <div>
          <InfoOutlined width={5} height={5} className="info-icon-outline" />
        </div>
        <div className="text-general">
          {tooltipText}
        </div>
      </Space>
    )
  };
  return (
    <Tooltip placement="topLeft" color="#FFFFFF" title={<ContentTooltip />}>
      <Typography className="sub-title">
        <span className="text-danger">
          {label}
        </span>
        <InfoOutlined width={5} height={5} className="info-icon" />
      </Typography>
    </Tooltip>
  )
}

export default Info;