import { ISVGProps } from "@interfaces/isvg";

function Upload({ style, width = 25, height = 25 }: ISVGProps) {
  return (
    <svg
      style={style}
      width={width}
      height={height}
      viewBox="0 0 25 25"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g clip-path="url(#clip0)">
        <path
          d="M16.5 16.66L12.5 12.66L8.5 16.66"
          stroke="#EE2E24"
          stroke-width="2"
          stroke-linecap="round"
          stroke-linejoin="round"
        />
        <path
          d="M12.5 12.66V21.66"
          stroke="#EE2E24"
          stroke-width="2"
          stroke-linecap="round"
          stroke-linejoin="round"
        />
        <path
          d="M20.8904 19.05C21.8658 18.5183 22.6363 17.6769 23.0803 16.6586C23.5244 15.6404 23.6167 14.5032 23.3427 13.4267C23.0686 12.3502 22.4439 11.3955 21.5671 10.7135C20.6903 10.0314 19.6113 9.66075 18.5004 9.66002H17.2404C16.9378 8.48926 16.3736 7.40235 15.5904 6.48101C14.8072 5.55967 13.8253 4.82787 12.7185 4.34063C11.6118 3.85338 10.409 3.62338 9.20057 3.6679C7.99213 3.71242 6.80952 4.03032 5.74163 4.59768C4.67374 5.16505 3.74836 5.96712 3.03507 6.9436C2.32178 7.92008 1.83914 9.04555 1.62343 10.2354C1.40772 11.4253 1.46455 12.6485 1.78966 13.8133C2.11477 14.978 2.69969 16.0539 3.50045 16.96"
          stroke="#EE2E24"
          stroke-width="2"
          stroke-linecap="round"
          stroke-linejoin="round"
        />
        <path
          d="M16.5 16.66L12.5 12.66L8.5 16.66"
          stroke="#EE2E24"
          stroke-width="2"
          stroke-linecap="round"
          stroke-linejoin="round"
        />
      </g>
      <defs>
        <clipPath id="clip0">
          <rect
            width="24"
            height="24"
            fill="white"
            transform="translate(0.5 0.660004)"
          />
        </clipPath>
      </defs>
    </svg>
  );
}

export default Upload;
