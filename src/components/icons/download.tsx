import { ISVGProps } from "@interfaces/isvg";

function Download({ style, width = 25, height = 25 }: ISVGProps) {
  return (
    <svg
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M8.25 17.25L12 21L15.75 17.25"
        stroke="#EE2E24"
        stroke-width="2"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M12 12V21"
        stroke="#EE2E24"
        stroke-width="2"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M20.4738 18C21.3032 17.4231 21.9252 16.5998 22.2495 15.6496C22.5737 14.6994 22.5835 13.6717 22.2772 12.7156C21.971 11.7596 21.3647 10.9249 20.5464 10.3328C19.7281 9.74065 18.7403 9.42184 17.7263 9.42263H16.5243C16.2374 8.3166 15.7005 7.28937 14.9542 6.41826C14.2078 5.54716 13.2714 4.85488 12.2153 4.39354C11.1593 3.93221 10.0113 3.71384 8.85762 3.75488C7.70394 3.79591 6.57471 4.09529 5.55494 4.63046C4.53517 5.16563 3.65142 5.92265 2.97024 6.84454C2.28906 7.76642 1.82819 8.82914 1.62233 9.9527C1.41647 11.0763 1.47098 12.2314 1.78176 13.3311C2.09254 14.4308 2.65149 15.4464 3.41653 16.3015"
        stroke="#EE2E24"
        stroke-width="2"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
    </svg>
  );
}

export default Download;
