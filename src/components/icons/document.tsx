import { ISVGProps } from "@interfaces/isvg";

function Document({ style, width = 25, height = 25 }: ISVGProps) {
  return (
    <svg
      style={style}
      width={width}
      height={height}
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M16.7708 1.5H7.22924C4.89349 1.5 3 3.70903 3 6.434V17.566C3 20.291 4.89349 22.5 7.22924 22.5H16.7708C19.1065 22.5 21 20.291 21 17.566V6.434C20.1985 5.47794 19.7777 4.9576 19.4447 4.54828C18.8429 3.8085 18.5278 3.43138 16.7708 1.5Z"
        stroke="#EE2E24"
        stroke-width="2"
        stroke-miterlimit="22.9256"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M11.25 6.75L6.75 6.75"
        stroke="#EE2E24"
        stroke-width="2"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M17.25 12H6.75"
        stroke="#EE2E24"
        stroke-width="2"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M17.25 17.25H6.75"
        stroke="#EE2E24"
        stroke-width="2"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M20.25 8.25H17.6913C16.205 8.25 15 6.64343 15 4.66163V2.25"
        stroke="#EE2E24"
        stroke-width="2"
        stroke-miterlimit="22.9256"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
    </svg>
  );
}

export default Document;
