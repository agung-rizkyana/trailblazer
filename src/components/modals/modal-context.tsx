import { directDownload } from "@helpers/download";
import { SubmitPayload } from "@libraries/api";
import React, { createContext, useContext, useEffect, useState } from "react";
import ModalDownload from "./modal-download";

export type TFileType = "pdf" | "xls" | "jpg";

export interface IModalDownloadContext {
  id: string;
  setId: (id: string) => void;
  doDownload: (id: string, file: TFileType) => void;
  open: boolean;
  setOpen: (open: boolean) => void;
  fileType: TFileType;
  setFileType: (fileType: TFileType) => void;
  makeDownload: (id: string) => void;
}

export const ModalDownloadContext = createContext({
  id: "",
  setId: () => {},
  doDownload: () => {},
  open: false,
  setOpen: () => {},
  fileType: "pdf",
  setFileType: (fileType: TFileType) => {},
  makeDownload: () => {},
} as IModalDownloadContext);

export function withModalDownload(WrapperComponent: React.FC) {
  const Content = () => {
    const [id, setId] = useState<string>("");
    const [open, setOpen] = useState<boolean>(false);
    const [fileType, setFileType] = useState<TFileType>("pdf");

    async function doDownload(id: string, file: TFileType) {
      try {
        const response = await SubmitPayload.download(id, file);
        directDownload(response, `download-report-${id}.${file}`);
      } catch (error) {
        console.log("error :: ", error);
        return error;
      }
    }

    async function makeDownload(id: string) {
      setId(id);
      setOpen(true);
    }

    return (
      <ModalDownloadContext.Provider
        value={{
          id,
          setId,
          doDownload,
          open,
          fileType,
          setOpen,
          setFileType,
          makeDownload,
        }}
      >
        {id && (
          <ModalDownload
            id={id}
            visible={open}
            onCancel={() => setOpen(false)}
          />
        )}
        <WrapperComponent />
      </ModalDownloadContext.Provider>
    );
  };

  return Content;
}
