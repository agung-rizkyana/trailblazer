import { Space, Modal, Typography, Button } from "antd";

import { Confirm } from "@components/icons";
import { PropsWithChildren } from "react";
import { IModalProps } from "./modal";
import { useMyModal } from "./modal.context";

interface IModalConfirm {
  school?: string;
}

function ModalConfirm(props: PropsWithChildren<IModalProps & IModalConfirm>) {
  const { close } = useMyModal();

  return (
    <Modal centered closable={false} footer={null} {...props}>
      <Space align="center" direction="vertical">
        <div>
          <Confirm />
        </div>
        <div className="text-center" style={{ margin: "1rem 0" }}>
          <Typography className="sub-title" children="Submit this form" />
          <Typography>
            Are you sure want to submit this form into{" "}
            <span className="text-bold">{props.school}</span>
          </Typography>
        </div>
        <div>
          <Button
            size="large"
            type="ghost"
            children="Cancel"
            onClick={props.onCancel}
            style={{ marginRight: "1rem" }}
          />
          <Button
            size="large"
            type="primary"
            children="Sure"
            onClick={props.onOk}
          />
        </div>
      </Space>
    </Modal>
  );
}

export default ModalConfirm;
