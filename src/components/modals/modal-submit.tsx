import {
  Space,
  Modal,
  ModalProps,
  Typography,
  Button,
  Row,
  Col,
  Radio,
  Form,
} from "antd";

import { School } from "@components/icons";
import { SCHOOLS } from "@containers/submit-forms/submit-form"; // TODO: change to constant folder
import { IRadio } from "@interfaces/iradio";
import { IModalProps } from "./modal";

function ModalSubmit(props: IModalProps) {
  return (
    <Modal centered closable={false} footer={null} width={1000} {...props}>
      <Space direction="horizontal" align="start">
        <div>
          <School />
        </div>
        <div>
          <Typography
            children="Choose school you want to submit this form : "
            className="sub-title"
          />
          <Form.Item name="unit_name">
            <Radio.Group>
              <Space direction="vertical" className="grid-cols-2">
                {SCHOOLS.map((r: IRadio, i: number) => (
                  <Radio value={r.value} key={i}>
                    {r.label}
                  </Radio>
                ))}
              </Space>
            </Radio.Group>
          </Form.Item>
        </div>
      </Space>
      <div className="text-right">
        <Button
          type="ghost"
          size="large"
          children="Cancel"
          style={{ marginRight: "1rem" }}
          onClick={props.onCancel}
        />
        <Button
          type="primary"
          size="large"
          children="Submit Form"
          onClick={props.onOk}
        />
      </div>
    </Modal>
  );
}

export default ModalSubmit;
