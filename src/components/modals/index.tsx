export { default as ModalFailed } from "./modal-failed";
export { default as ModalSubmit } from "./modal-submit";
export { default as ModalConfirm } from "./modal-confirm";
export { default as ModalSuccess } from "./modal-success";
export { default as ModalDownload } from "./modal-download";
export { default as withModal } from "./modal.context";
