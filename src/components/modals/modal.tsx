import { ModalProps } from "antd";
import { PropsWithChildren } from "react";
import ModalFailed from "./modal-failed";
import ModalSuccess from "./modal-success";
import ModalConfirm from "./modal-confirm";
import ModalSubmit from "./modal-submit";

export type TVariants = "success" | "failed" | "confirm" | "submit";

export interface IModalProps extends ModalProps {
  variants?: TVariants;
}

function Modal(props: PropsWithChildren<IModalProps>) {
  const renderVariants = (): JSX.Element => {
    switch (props.variants) {
      case "failed":
        return <ModalFailed {...props} />;
      case "confirm":
        return <ModalConfirm {...props} />;
      case "submit":
        return <ModalSubmit {...props} />;
      default:
        return <ModalSuccess {...props} />;
    }
  };

  return renderVariants();
}

export default Modal;
