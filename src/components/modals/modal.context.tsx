import { ModalProps } from "antd";
import { createContext, useContext, useState } from "react";
import MyModal, { IModalProps } from "./modal";

export interface IModalContext {
  modal: IModalProps;
  setModal(modal: ModalProps & IModalProps): void;
}

const initialValues: IModalContext = {
  modal: {} as IModalProps,
  setModal(modal: IModalProps) {},
};

export const ModalContext = createContext(initialValues);
export const useMyModal = () => {
  const [show, setShow] = useState<boolean>(true);
  const { setModal } = useContext(ModalContext);
  const failed = (modal?: IModalProps) => {
    setModal({
      ...modal,
      visible: show,
      variants: "failed",
    });
  };

  const succces = (modal?: IModalProps) => {
    setModal({
      ...modal,
      visible: show,
      variants: "success",
    });
  };

  const submit = (modal?: IModalProps) => {
    setModal({
      ...modal,
      visible: show,
      variants: "submit",
    });
  };

  const confirm = (modal?: IModalProps) => {
    setModal({
      ...modal,
      visible: show,
      variants: "confirm",
    });
  };

  const close = () => {
    setModal({
      visible: false,
    });
  };

  return {
    failed,
    submit,
    succces,
    confirm,
    close,
  };
};

export function withModal(WrappedComponent: React.FC) {
  const Content = () => {
    const [modal, setModal] = useState<IModalProps>({} as IModalProps);
    return (
      <ModalContext.Provider
        value={{
          modal,
          setModal,
        }}
      >
        <WrappedComponent />
        <MyModal {...modal} />
      </ModalContext.Provider>
    );
  };

  return Content;
}

export default withModal;
