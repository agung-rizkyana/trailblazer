import { Space, Modal, Typography, Button } from "antd";

import { Tasks } from "@components/icons";
import { PropsWithChildren, useState } from "react";
import { IModalProps } from "./modal";
import { useMyModal } from "./modal.context";
import { TFileType } from "./modal-context";
import { SubmitPayload } from "@libraries/api";
import { directDownload } from "@helpers/download";

interface IModalDownload {
  school?: string;
  id: string;
  // handleDownload: (id: string, file: TFileType) => void;
}

function ModalDownload(props: PropsWithChildren<IModalProps & IModalDownload>) {
  const [loading, setLoading] = useState<{
    [key: string]: boolean;
  }>({ pdf: false });
  const { close } = useMyModal();

  async function doDownload(id: string, file: TFileType) {
    setLoading({
      [file]: true,
    });
    try {
      const response = await SubmitPayload.download(id, file);
      directDownload(response.data, `download-report-${id}.${file}`);
      setLoading({
        [file]: false,
      });
    } catch (error) {
      console.log("error :: ", error);
      setLoading({
        [file]: false,
      });
      return error;
    }
  }

  return (
    <Modal {...props} centered closable={false} footer={null} width={350}>
      <div
        style={{
          display: "flex",
          flexDirection: "column",

          // alignItems: "center",
        }}
      >
        <div style={{ flex: 1, textAlign: "center" }}>
          <Tasks />
        </div>
        <div style={{ textAlign: "center", margin: "1.5rem 0" }}>
          <Typography
            style={{ fontWeight: "bold", marginBottom: "1rem" }}
            children="Choose download type"
          />
          <Typography children="Select the type you want to download" />
        </div>
        <div>
          <Button
            block
            size="large"
            type="primary"
            children="PDF"
            onClick={() => doDownload(props.id, "pdf")}
            // onClick={props.handleDownload}
            style={{ margin: ".5rem 0" }}
            loading={loading["pdf"]}
          />
          <Button
            block
            size="large"
            type="primary"
            children="XLS"
            onClick={() => doDownload(props.id, "xls")}
            // onClick={props.handleDownload}
            style={{ margin: ".5rem 0" }}
            loading={loading["xls"]}
          />
          <Button
            block
            size="large"
            type="primary"
            children="JPG"
            onClick={() => doDownload(props.id, "jpg")}
            // onClick={props.handleDownload}
            style={{ margin: ".5rem 0" }}
            loading={loading["jpg"]}
          />
          <Button
            block
            size="large"
            type="default"
            children="Cancel Download"
            onClick={props.onCancel}
            style={{ margin: ".5rem 0" }}
          />
        </div>
      </div>
    </Modal>
  );
}

export default ModalDownload;
