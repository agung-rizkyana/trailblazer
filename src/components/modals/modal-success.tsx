import { Space, Modal, Typography, Button } from "antd";

import { Success } from "@components/icons";
import { IModalProps } from "./modal";
import { useMyModal } from "./modal.context";

function ModalSuccess(props: IModalProps) {
  const { close } = useMyModal();
  return (
    <Modal centered closable={false} width={400} footer={null} {...props}>
      <Space align="center" direction="vertical" style={{ width: "100%" }}>
        <div>
          <Success />
        </div>
        <div className="text-center" style={{ margin: "1rem 0" }}>
          <Typography className="sub-title" children="Submit Form Success!" />
          <Typography>
            Your Form is successfully submited Please, check you email!
          </Typography>
        </div>
        <div>
          <Button
            size="large"
            type="primary"
            children="Yay, thank you!"
            style={{ width: "10rem" }}
            onClick={() => close()}
          />
        </div>
      </Space>
    </Modal>
  );
}

export default ModalSuccess;
