import { Space, Modal, ModalProps, Typography, Button } from "antd";

import { Failed } from "@components/icons";

function ModalFailed(props: ModalProps) {
  return (
    <Modal centered closable={false} footer={null} {...props}>
      <Space align="center" direction="vertical">
        <div>
          <Failed />
        </div>
        <div className="text-center" style={{ margin: "1rem 0" }}>
          <Typography
            className="sub-title"
            children="Oops! The form isn't complete!"
          />
          <Typography>
            It looks like you didn’t fill all the form, please complete the form
            before you submit.
          </Typography>
        </div>
        <div>
          <Button
            size="large"
            type="primary"
            children="Return to Form"
            style={{ width: "15rem" }}
            onClick={props.onOk}
          />
        </div>
      </Space>
    </Modal>
  );
}

export default ModalFailed;
