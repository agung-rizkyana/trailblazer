import { Layout, Breadcrumb } from "antd";
import React, { PropsWithChildren } from "react";
import { NavbarAuth as Navbar } from "@components/navbar";
import { useEffect } from "react";
import AuthHelper from "@helpers/auth";
import { useHistory } from "react-router-dom";

interface IPageProps {
  breadcrumbs?: string[];
}

const { Content } = Layout;

function AuthLayout({ children }: PropsWithChildren<any>) {
  const history = useHistory();
  useEffect(() => {
    if (AuthHelper.getToken()) {
      history.replace("/submit-form");
    }
  }, [AuthHelper.getToken()]);
  return (
    <Layout className="auth-layout">
      <Navbar />
      <Layout>
        <Layout style={{ padding: "0 24px 24px" }}>{children}</Layout>
      </Layout>
    </Layout>
  );
}

export function withAuthPage(WrappedComponent: React.FC) {
  const ContentPage = (props: IPageProps): JSX.Element => {
    return (
      <AuthLayout>
        <Content className="site-layout-background content">
          <WrappedComponent />
        </Content>
      </AuthLayout>
    );
  };
  return ContentPage;
}

export default AuthLayout;
