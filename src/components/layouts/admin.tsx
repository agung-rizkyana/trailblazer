import { Layout, Breadcrumb } from 'antd';
import React, { PropsWithChildren } from 'react';
import Navbar from '@components/navbar';
import Sidebar from '@components/sidebar';
import { useEffect } from 'react';
import AuthHelper from '@helpers/auth';
import { useHistory } from 'react-router-dom';

interface IPageProps {
  breadcrumbs?: string[];
}

const { Content } = Layout;

function AdminLayout({ children }: PropsWithChildren<any>) {
  const history = useHistory();
  useEffect(() => {
    if (!AuthHelper.getToken()) {
      history.replace('/login');
    }
  }, [AuthHelper.getToken()]);
  return (
    <Layout className="base-layout">
      <Navbar />
      <Layout>
        <Sidebar />
        <Layout style={{ padding: '0 24px 24px' }}>
          {children}
        </Layout>
      </Layout>
    </Layout>
  )
}

export function withAdminPage(WrappedComponent: React.FC, props: IPageProps) {
  const ContentPage = (props: IPageProps): JSX.Element => {
    return (
      <AdminLayout>
        {
          props.breadcrumbs && (
            <Breadcrumb style={{ margin: '16px 0' }}>
              {
                props.breadcrumbs.map((b: string, i: number) => (
                  <Breadcrumb.Item key={i}>
                    {b}
                  </Breadcrumb.Item>
                ))
              }
            </Breadcrumb>
          )
        }

        <Content
          className="site-layout-background content"
        >
          <WrappedComponent />
        </Content>
      </AdminLayout>
    )
  }
  return ContentPage;
}

export default AdminLayout;
