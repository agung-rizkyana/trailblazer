import {
  Menu,
  Dropdown,
} from 'antd';

import { Profile } from '@components/icons';

import AuthHelper from '@helpers/auth';

function AccountMenu() {

  const doLogout = () => {
    AuthHelper.clearToken();
    window.location.reload();
  };

  return (
    <Menu>
      <Menu.Item key="view-profile">
        View Profile
      </Menu.Item>
      <Menu.Item key="edit-profile">
        Edit Profile
      </Menu.Item>
      <Menu.Item key="drafts">
        Drafts
      </Menu.Item>
      <Menu.Item key="change-password">
        Change Passwords
      </Menu.Item>
      <Menu.Item key="logout" onClick={() => doLogout()}>
        Log Out
      </Menu.Item>
    </Menu>
  )
}

function Account() {
  return (
    <Dropdown overlay={AccountMenu}>
      <div className="navbar-menu-item">
        <Profile />
      </div>
    </Dropdown>
  )
}

export default Account;
