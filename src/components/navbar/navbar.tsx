import React from 'react';
import { Layout, Menu, Breadcrumb, Space, Row, Col, Input } from 'antd';
import { Logo, Profile, Notification, BurgerMenu } from '@components/icons';

import { SearchOutlined } from '@ant-design/icons';

import AccountMenu from './libraries/account';

const { SubMenu } = Menu;
const { Header, Content, Sider } = Layout;

function Navbar() {
  return (
    <Header className="navbar">
      <Row gutter={12} align="middle">
        <Col span={8}>
          <div className="logo" >
            <BurgerMenu style={{ marginRight: '1.5rem' }} />
            <Logo />
          </div>
        </Col>
        <Col span={8}>
          <div className="input-search">
            <Input size="large" placeholder="Search" prefix={<SearchOutlined />} />
          </div>
        </Col>
        <Col span={8}>
          <div className="navbar-menu">
            <div className="navbar-menu-item">
              <a href="">
                <Notification />
              </a>
            </div>
            <AccountMenu />
          </div>
        </Col>

        {/* <Menu theme="light" mode="horizontal" defaultSelectedKeys={['2']}>
          <Menu.Item key="1">nav 1</Menu.Item>
          <Menu.Item key="2">nav 2</Menu.Item>
          <Menu.Item key="3">nav 3</Menu.Item>
        </Menu> */}
      </Row>


    </Header>
  )
}

export default Navbar;