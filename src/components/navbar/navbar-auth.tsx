import React from 'react';
import { Layout, Menu, Breadcrumb, Space, Row, Col, Input, Button } from 'antd';
import { Logo, Profile, Notification, BurgerMenu } from '@components/icons';

import { SearchOutlined } from '@ant-design/icons';

const { SubMenu } = Menu;
const { Header, Content, Sider } = Layout;

function Navbar() {
  return (
    <Header className="navbar-auth">
      <Row gutter={12} align="middle">
        <Col span={18} >
          <div className="logo-auth">
            <Logo />
          </div>
        </Col>

        <Col span={6} className="text-right">
          <Button
            type="ghost"
            children="Sign up"
            className="btn-signup"
            size="large"
          />
        </Col>

        {/* <Menu theme="light" mode="horizontal" defaultSelectedKeys={['2']}>
          <Menu.Item key="1">nav 1</Menu.Item>
          <Menu.Item key="2">nav 2</Menu.Item>
          <Menu.Item key="3">nav 3</Menu.Item>
        </Menu> */}
      </Row>


    </Header>
  )
}

export default Navbar;