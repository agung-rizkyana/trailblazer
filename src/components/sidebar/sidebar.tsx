import {
  UserOutlined,
  LaptopOutlined,
  NotificationOutlined,
} from "@ant-design/icons";
import { Layout, Menu } from "antd";
import { useState } from "react";
import { useHistory } from "react-router-dom";

const { SubMenu } = Menu;
const { Sider } = Layout;

function Sidebar() {
  const history = useHistory();
  const pathname = history.location.pathname.split("/")[1]; // current url on first
  const [activeMenu, setActiveMenu] = useState<string>(pathname);
  return (
    <Sider width={200} className="site-layout-background sidebar">
      <Menu
        mode="inline"
        selectedKeys={[activeMenu]}
        // defaultSelectedKeys={['sub2']}
        // defaultOpenKeys={['sub2']}
        style={{ height: "100%", borderRight: 0 }}
        onSelect={({ item, key, keyPath, selectedKeys, domEvent }) => {
          if (key === pathname) {
            setActiveMenu(key);
          }
        }}
      >
        <Menu.Item key="dashboard" icon={<UserOutlined />} title="Dashboard">
          Dashboard
          {/* <Menu.Item key="1">option1</Menu.Item>
          <Menu.Item key="2">option2</Menu.Item>
          <Menu.Item key="3">option3</Menu.Item>
          <Menu.Item key="4">option4</Menu.Item> */}
        </Menu.Item>
        <Menu.Item
          key="submit-form"
          icon={<LaptopOutlined />}
          title="Submit Form"
          onClick={() => history.push("/submit-form")}
        >
          Submit Form
          {/* <Menu.Item key="5">option5</Menu.Item>
          <Menu.Item key="6">option6</Menu.Item>
          <Menu.Item key="7">option7</Menu.Item>
          <Menu.Item key="8">option8</Menu.Item> */}
        </Menu.Item>
        <Menu.Item
          key="report"
          icon={<NotificationOutlined />}
          title="Reports"
          onClick={() => history.push("/report")}
        >
          Report
          {/* <Menu.Item key="9">option9</Menu.Item>
          <Menu.Item key="10">option10</Menu.Item>
          <Menu.Item key="11">option11</Menu.Item>
          <Menu.Item key="12">option12</Menu.Item> */}
        </Menu.Item>
        <Menu.Item
          key="role-management"
          icon={<NotificationOutlined />}
          title="Role Management"
        >
          Role Management
          {/* <Menu.Item key="9">option9</Menu.Item>
          <Menu.Item key="10">option10</Menu.Item>
          <Menu.Item key="11">option11</Menu.Item>
          <Menu.Item key="12">option12</Menu.Item> */}
        </Menu.Item>
      </Menu>
    </Sider>
  );
}

export default Sidebar;
