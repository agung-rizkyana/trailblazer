export interface ISubmitForm {
  division: string;
  division_name?: string;
  division_id?: string;
  function_unit_name?: string;
  function_unit_id?: string;
  cfu_fu: string;
  whys: { why: string }[];
  why?: string[];
  org_whats: { organization_what: string }[];
  organization_what?: string[];
  ind_whats: { individual_what: string }[];
  individual_what?: string[];
  ubis_participants: string;
  ubis_budget: string;
  ubis_days: string;
  ubis_other_info: string;
  individual_band: string;
  individual_age: string;
  individual_unit: string;
  individual_other_info: string;
  success: { success_keys: string }[];
  success_keys?: string[];
  training_name: string;
  training_date: string;
  strategic_plan: string;
  ubis_goal: string;
  individual_goal: string;
  skills: { objective_skills: string }[];
  objective_skills?: string[];
  delivery_method: string;
  learning_styles: string[];
  unit_name?: string; // akan diubah menjadi unit_id
  status?: -1 | 1 | 0 | 2; // status : -1 {Draft}, 0 {Learning Analyst}, 1 {Design}, 2 {Development} ...
  is_submitted?: boolean;
}
