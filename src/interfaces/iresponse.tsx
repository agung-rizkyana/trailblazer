export type TPagination = {
  page: number;
  per: number;
  total: number;
}

export interface IResponse {
  success: boolean;
  error_code?: string;
  reason?: string;
  data: any;
  pagination?: TPagination;
}