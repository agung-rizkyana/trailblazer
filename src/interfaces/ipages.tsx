export interface IPage {
  component: any;
  path: string;
  childs?: IPages;
}

export interface IPages {
  group: string;
  icon?: any;
  pages: IPage[];
}
