export interface ISVGProps {
  style?: React.CSSProperties;
  width?: number;
  height?: number;
}