export interface IFormRulesItem {
  required?: boolean;
  message?: string;
}
