const CracoLessPlugin = require('craco-less');
const path = require('path');

module.exports = {
  webpack: {
    alias: {
      '@assets': path.resolve(__dirname, "./src/assets/"),
      '@components': path.resolve(__dirname, "./src/components/"),
      '@config': path.resolve(__dirname, "./src/config/"),
      '@containers': path.resolve(__dirname, "./src/containers/"),
      '@dummies': path.resolve(__dirname, "./src/dummies/"),
      '@helpers': path.resolve(__dirname, "./src/helpers/"),
      '@interfaces': path.resolve(__dirname, "./src/interfaces/"),
      '@libraries': path.resolve(__dirname, "./src/libraries/"),
      '@pages': path.resolve(__dirname, "./src/pages/"),
      '@services-worker': path.resolve(__dirname, "./src/services-worker/"),
      '@context': path.resolve(__dirname, "./src/context/"),
      '@fragments': path.resolve(__dirname, "./src/fragments/"),
      '@layouts': path.resolve(__dirname, "./src/layouts/"),

    }
  },
  plugins: [
    {
      plugin: CracoLessPlugin,
      options: {
        lessLoaderOptions: {
          lessOptions: {
            modifyVars: {
              '@primary-color': '#EE2E24'
            },
            javascriptEnabled: true,

          }
        }
      }
    }
  ],
};